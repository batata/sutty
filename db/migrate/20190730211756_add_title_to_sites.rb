# frozen_string_literal: true

# Agrega el título al sitio
class AddTitleToSites < ActiveRecord::Migration[5.2]
  def change
    add_column :sites, :title, :string
  end
end
