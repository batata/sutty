# frozen_string_literal: true

# Un campo numérico
class MetadataNumber < MetadataTemplate
  # Nada
  def default_value
    super || nil
  end

  def save
    return true unless changed?

    self[:value] = value.to_i
    self[:value] = encrypt(value) if private?

    true
  end

  private

  def decrypt(value)
    super(value).to_i
  end
end
