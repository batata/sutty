#!/bin/sh
set -e

cd /srv/http

for patch in /tmp/patches-${1}/*.patch; do
  su -c "patch -Np 1 -i ${patch}" app && rm $patch
done

cat tmp/puma.pid | xargs -r kill -USR2
