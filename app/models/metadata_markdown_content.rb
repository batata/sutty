# frozen_string_literal: true

# Contenido con el editor de Markdown
class MetadataMarkdownContent < MetadataText
  # Renderizar a HTML y sanitizar
  def to_s
    sanitize CommonMarker.render_doc(value, %i[FOOTNOTES SMART],
                                     %i[table strikethrough autolink]).to_html
  end

  def value
    self[:value] || document_value || default_value
  end

  def front_matter?
    false
  end

  # @return [String]
  def document_value
    document.content
  end

  # XXX: No sanitizamos acá porque se escapan varios símbolos de
  # markdown y se eliminan autolinks.  Mejor es deshabilitar la
  # generación SAFE de CommonMark en la configuración del sitio.
  def sanitize(string)
    string.tr("\r", '').unicode_normalize
  end
end
