# frozen_string_literal: true

class ActivityPub
  class Activity
    class Delete < ActivityPub::Activity
      # Los Delete se refieren a objetos.  Al eliminar un objeto,
      # cancelamos todas las actividades que tienen relacionadas.
      #
      # XXX: La actividad tiene una firma, pero la implementación no
      # está recomendada
      #
      # @todo Validar que le Actor corresponda con los objetos. Esto ya
      # lo haría la Social Inbox por nosotres.
      # @see {https://docs.joinmastodon.org/spec/security/#ld}
      def update_activity_pub_state!
        ActiveRecord::Base.connection_pool.with_connection do
          ActivityPub.transaction do
            object = ActivityPub::Object.find_by(uri: ActivityPub.uri_from_object(content['object']))

            if object.present?
              object.activity_pubs.find_each do |activity_pub|
                activity_pub.remove! if activity_pub.may_remove?
              end

              # Encontrar todas las acciones de moderación de le actore
              # eliminade y moverlas a eliminar.
              if (actor = ActivityPub::Actor.find_by(uri: object.uri)).present?
                ActorModeration.where(actor_id: actor.id).remove_all!
              end
            end

            activity_pub.remove! if activity_pub.may_remove?
          end
        end
      end
    end
  end
end
