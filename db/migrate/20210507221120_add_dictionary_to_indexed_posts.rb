# frozen_string_literal: true

# Para no estar calculando todo el tiempo el diccionario del idioma,
# agregamos una columna más.
class AddDictionaryToIndexedPosts < ActiveRecord::Migration[6.1]
  LOCALES = {
    'english' => 'en',
    'spanish' => 'es'
  }

  def up
    add_column :indexed_posts, :dictionary, :string

    create_trigger(compatibility: 1).on(:indexed_posts).before(:insert, :update) do
      "new.indexed_content := to_tsvector(('pg_catalog.' || new.dictionary)::regconfig, coalesce(new.title, '') || '\n' || coalesce(new.content,''));"
    end

    IndexedPost.find_each do |post|
      locale = post.locale

      post.update dictionary: locale, locale: LOCALES[locale]
    end
  end

  def down
    remove_column :indexed_posts, :locale
    rename_column :indexed_posts, :dictionary, :locale
  end
end
