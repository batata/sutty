# frozen_string_literal: true

module Constraints
  # Detecta si el dominio comienzo con api. para servir la API.
  #
  # Hacemos esto porque Rails históricamente tiene un largo fijo de TLD
  # y como alojamos dominios que pueden tener distintas terminaciones,
  # no siempre detecta el subdominio como corresponde.
  #
  # Antes de llegar a este punto tenemos que tener un certificado
  # correspondiente en el servidor web, que se expide dentro del
  # servidor, por lo que sería la primera línea para detener
  # api.cualquiercosa.que.no.existe.org si hiciera falta.
  class ApiSubdomain
    API_SUBDOMAIN = 'api.'

    def initialize; end

    # Sólo verificamos que el subdominio empiece con api.
    def matches?(request)
      request.hostname.start_with? API_SUBDOMAIN
    end
  end
end
