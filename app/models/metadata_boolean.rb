# frozen_string_literal: true

# Implementa valores por sí o por no
#
# Esto es increíblemente difícil de lograr que salga bien!
class MetadataBoolean < MetadataTemplate
  # El valor por defecto es una versión booleana de lo que diga (o no
  # diga) el esquema
  #
  # @return [Boolean]
  def default_value
    !!super
  end

  # Los checkboxes son especiales porque la especificación de HTML
  # indica que no se envían aquellos checkboxes con valores vacíos, con
  # lo que nunca sabemos cuándo se los está deshabilitando.  Rails
  # subsana esto enviando un valor vacío bajo el mismo nombre
  # (generalmente '0').
  #
  # En este caso, queremos priorizar el dato enviado por le usuarie
  # antes que el generado internamente.
  #
  # Tenemos varios casos:
  #
  # * nil => false
  # * '0' => false
  # * '1' => true
  # * false
  # * true
  def value
    case self[:value]
    when NilClass
      document.data.fetch(name.to_s, default_value)
    when String
      true_values.include? self[:value]
    else
      self[:value]
    end
  end

  # Siempre guardar el valor de este campo a menos que sea nulo
  def empty?
    value.nil?
  end

  private

  # Los valores que evalúan a verdadero
  #
  # XXX: memoizamos porque Struct no puede declarar constantes.
  def true_values
    @true_values ||= %w[1 on true].freeze
  end
end
