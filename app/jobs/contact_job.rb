# frozen_string_literal: true

# Envía los mensajes de contacto
class ContactJob < ApplicationJob
  # @param [Integer]
  # @param [String]
  # @param [Hash]
  def perform(site, form_name, form, origin = nil)
    @site = site

    # Sanitizar los valores
    form.each_key do |key|
      form[key] = ActionController::Base.helpers.sanitize form[key]
    end

    # Enviar de a 10 usuaries para minimizar el riesgo que nos
    # consideren spammers.
    #
    # TODO: #i18n. Agrupar usuaries por su idioma

    usuaries.each_slice(10) do |u|
      ContactMailer.with(form_name: form_name,
                         form: form,
                         site: site,
                         usuaries_emails: u,
                         origin: origin)
                   .notify_usuaries.deliver_now
    end
  end

  private

  # Trae solo les usuaries definitives para eliminar un vector de ataque
  # donde alguien crea un sitio, agrega a muches usuaries y les envía
  # correos.
  #
  # TODO: Mover a Site#usuaries
  def usuaries
    site.roles.where(rol: 'usuarie', temporal: false).includes(:usuarie)
        .pluck(:email)
  end
end
