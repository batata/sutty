# frozen_string_literal: true

# Como la instancia es única para todo el panel, necesitamos llevar
# registro de su relación con cada sitio por separado.
class CreateInstanceModeration < ActiveRecord::Migration[6.1]
  def up
    create_table :instance_moderations do |t|
      t.timestamps

      t.belongs_to :site
      t.uuid :instance_id, index: true

      t.string :aasm_state, null: false, default: 'paused'

      t.index %i[site_id instance_id], unique: true
    end

    ActivityPub.all.find_each do |activity_pub|
      InstanceModeration.find_or_create_by(site: activity_pub.site, instance: activity_pub.instance)
    end
  end

  def down
    drop_table :instance_moderations
  end
end
