# frozen_string_literal: true

class ActivityPub
  class Activity
    # Boost
    class Announce < ActivityPub::Activity; end
  end
end
