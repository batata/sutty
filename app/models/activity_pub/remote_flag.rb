# frozen_string_literal: true

class ActivityPub
  class RemoteFlag < ApplicationRecord
    IGNORED_EVENTS = [].freeze
    IGNORED_STATES = [].freeze

    include AASM

    aasm do
      state :waiting, initial: true
      state :queued
      state :sent

      event :queue do
        transitions from: :waiting, to: :queued
      end

      event :report do
        transitions from: :queued, to: :sent
      end

      event :resend do
        transitions from: :sent, to: :waiting
      end
    end

    # Definir eventos en masa
    include AasmEventsConcern

    belongs_to :actor
    belongs_to :site

    has_one :actor_moderation
    has_many :activity_pubs
    # XXX: source_type es obligatorio para el `through`
    has_many :objects, through: :activity_pubs, source_type: 'ActivityPub::Object::Note'

    # Genera la actividad a enviar
    def content
      {
        '@context' => 'https://www.w3.org/ns/activitystreams',
        'id' => Rails.application.routes.url_helpers.v1_activity_pub_remote_flag_url(self,
                                                                                     host: site.social_inbox_hostname),
        'type' => 'Flag',
        'actor' => main_site.social_inbox.actor_id,
        'content' => message.to_s,
        'object' => [actor.uri] + objects.pluck(:uri)
      }
    end

    # Este es el sitio principal que actúa como origen del reporte.
    # Tiene que tener la Social Inbox habilitada al mismo tiempo.
    #
    # @return [Site]
    def main_site
      @main_site ||= Site.find(ENV.fetch('PANEL_ACTOR_SITE_ID', 1))
    end
  end
end
