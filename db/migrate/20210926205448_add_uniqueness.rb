# frozen_string_literal: true

# Agrega índices únicos que pensábamos que ya existían.
class AddUniqueness < ActiveRecord::Migration[6.1]
  def change
    add_index :designs, :name, unique: true
    add_index :designs, :gem, unique: true
    add_index :licencias, :name, unique: true
  end
end
