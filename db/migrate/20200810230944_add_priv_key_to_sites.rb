# frozen_string_literal: true

# Agrega las llaves privadas cifradas a cada sitio
class AddPrivKeyToSites < ActiveRecord::Migration[6.0]
  def up
    add_column :sites, :private_key_ciphertext, :string

    Site.find_each do |site|
      site.update_attribute :private_key, Lockbox.generate_key
    end
  end

  def down
    remove_column :sites, :private_key_ciphertext
  end
end
