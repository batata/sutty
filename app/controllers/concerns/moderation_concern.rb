# frozen_string_literal: true

module ModerationConcern
  extend ActiveSupport::Concern

  included do
    private

    def redirect_to_moderation_queue!
      redirect_back fallback_location: site_moderation_queue_path(**(session[:moderation_queue_filters] || {}))
    end

    # @return [String]
    def panel_actor_mention
      @panel_actor_mention ||= ENV.fetch('PANEL_ACTOR_MENTION', '@sutty@sutty.nl')
    end

    def remote_flag_params(model)
      remote_flag = ActivityPub::RemoteFlag.find_by(actor_id: model.actor_id)

      { remote_flag_attributes: { id: remote_flag&.id, message: ''.dup } }.tap do |p|
        p[:remote_flag_attributes][:site_id] = model.site_id
        p[:remote_flag_attributes][:actor_id] = model.actor_id

        I18n.available_locales.each do |locale|
          p[:remote_flag_attributes][:message].tap do |m|
            m << I18n.t(locale)
            m << ': '
            m << I18n.t('remote_flags.report_message', locale: locale, panel_actor_mention: panel_actor_mention)
            m << '\n\n'
          end
        end
      end
    end
  end
end
