# frozen_string_literal: true

class AccessLog < ApplicationRecord
  # Las peticiones completas son las que terminaron bien y se
  # respondieron con 200 OK o 304 Not Modified
  #
  # @see {https://en.wikipedia.org/wiki/List_of_HTTP_status_codes}
  scope :completed_requests, -> { where(request_method: 'GET', request_completion: 'OK', status: [200, 304]) }
  scope :non_robots, -> { where(crawler: false) }
  scope :robots, -> { where(crawler: true) }
  scope :pages, -> { where(sent_http_content_type: ['text/html', 'text/html; charset=utf-8', 'text/html; charset=UTF-8']) }
end
