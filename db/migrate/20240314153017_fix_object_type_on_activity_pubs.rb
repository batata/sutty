# frozen_string_literal: true

# Arregla la relación rota entre ActivityPub y Objects
class FixObjectTypeOnActivityPubs < ActiveRecord::Migration[6.1]
  def up
    ActivityPub::Object.where.not(type: 'ActivityPub::Object::Generic').find_each do |object|
      ActivityPub.where(object_id: object.id).update_all(object_type: object.type, updated_at: Time.now)
    end
  end

  def down; end
end
