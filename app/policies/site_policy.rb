# frozen_string_literal: true

# Política de acceso para sitios
class SitePolicy
  attr_reader :site, :usuarie

  def initialize(usuarie, site)
    @usuarie = usuarie
    @site = site
  end

  # Todes les usuaries pueden ver sus propios sitios
  def index?
    true
  end

  def status?
    true
  end

  # Puede ver la versión privada del sitio?
  def private?
    edit? && site.deploys.find_by_type('DeployPrivate')
  end

  # Todes les usuaries pueden ver el sitio si aceptaron la invitación
  def show?
    !current_role.temporal
  end

  # Todes pueden ver los archivos
  def static_file?
    true
  end

  # Todes pueden crear nuevos sitios
  def new?
    true
  end

  def create?
    new?
  end

  # Para poder editarlos también tienen que haber aceptado la invitación
  def edit?
    show? && usuarie?
  end

  def update?
    edit?
  end

  def destroy?
    edit?
  end

  # Les invitades no pueden generar el sitio y les usuaries solo hasta
  # que aceptan la invitación
  def build?
    show? && usuarie?
  end

  def button?
    show?
  end

  def enqueue?
    build?
  end

  def reorder_posts?
    build?
  end

  def pull?
    build?
  end

  def fetch?
    pull?
  end

  def merge?
    pull?
  end

  # Solo les usuaries pueden reordenar artículos
  def reorder?
    site.usuarie? usuarie
  end

  private

  def current_role
    usuarie.rol_for_site(site)
  end

  def usuarie?
    site.usuarie? usuarie
  end

  def invitade?
    site.invitade? usuarie
  end
end
