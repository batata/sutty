# frozen_string_literal: true

# Algunos diseños están deshabilitados
class AddDisabledToDesigns < ActiveRecord::Migration[5.2]
  def change
    add_column :designs, :disabled, :boolean, default: false
  end
end
