# frozen_string_literal: true

require 'test_helper'

class SitesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rol = create :rol
    @site = @rol.site
    @usuarie = @rol.usuarie

    @authorization = {
      Authorization: ActionController::HttpAuthentication::Basic
                     .encode_credentials(@usuarie.email, @usuarie.password)
    }
  end

  teardown do
    @site.destroy
  end

  test 'se pueden ver' do
    get sites_url, headers: @authorization

    assert_match @site.name, response.body
  end

  test 'se puede ver el formulario de creación' do
    get new_site_url, headers: @authorization

    assert_match(/<form.*id="new_site"/, response.body)
  end

  test 'se pueden crear' do
    name = SecureRandom.hex
    design = Design.all.where.not(gem: Design::NO_THEMES).sample

    post sites_url, headers: @authorization, params: {
      site: {
        name: name,
        title: name,
        description: name * 2,
        design_id: design.id,
        licencia_id: Licencia.first.id,
        deploys_attributes: {
          '0' => {
            type: 'DeployLocal'
          }
        }
      }
    }

    site = Site.find_by_name(name)

    assert site
    assert_equal @usuarie.email, site.roles.first.usuarie.email
    assert_equal 'usuarie', site.roles.first.rol

    assert_equal name, site.name
    assert_equal name, site.title
    assert_equal name * 2, site.description
    assert_equal design, site.design
    assert_equal design.gem, site.config.theme
    assert_equal name, site.config.title
    assert_equal name * 2, site.config.description

    # TODO: Esto requiere que podamos buscar hacia atrás en la historia
    # del repositorio
    # assert_equal I18n.t('site_service.create', name: name),
    #             site.repository.rugged.head.target.message

    I18n.available_locales.each do |locale|
      assert_equal 3, site.posts(lang: locale).size
    end

    site.destroy
  end

  test 'no se pueden crear con cualquier deploy' do
    name = SecureRandom.hex

    assert_raise ActiveRecord::SubclassNotFound do
      post sites_url, headers: @authorization, params: {
        site: {
          name: name,
          design_id: create(:design).id,
          licencia_id: create(:licencia).id,
          deploys_attributes: {
            '0' => {
              type: 'DeployNoExiste'
            }
          }
        }
      }
    end
  end

  test 'se pueden encolar' do
    post site_enqueue_url(@site), headers: @authorization

    assert File.directory?(@site.deploys.first.destination)
    assert File.exist?(File.join(@site.deploys.first.destination,
                                 'index.html'))
  end

  test 'no se pueden encolar varias veces seguidas' do
    assert_enqueued_jobs 2 do
      post site_enqueue_url(@site), headers: @authorization
      post site_enqueue_url(@site), headers: @authorization
    end
  end

  test 'se pueden actualizar' do
    name = SecureRandom.hex
    design = Design.all.where.not(id: @site.design_id).sample

    put site_url(@site), headers: @authorization, params: {
      site: {
        name: name,
        title: name,
        description: name * 2,
        design_id: design.id,
        licencia_id: Licencia.all.second.id,
        deploys_attributes: {
          '0' => {
            type: 'DeployLocal'
          }
        }
      }
    }

    @site = Site.find(@site.id)

    assert_equal name, @site.name
    assert_equal name, @site.title
    assert_equal name * 2, @site.description
    assert_equal design, @site.design
    assert_equal design.gem, @site.config.theme
    assert_equal name, @site.config.title
    assert_equal name * 2, @site.config.description
    # assert_equal I18n.t('site_service.update', name: name),
    #             @site.repository.rugged.head.target.message

    I18n.available_locales.each do |locale|
      Mobility.with_locale(locale) do
        assert @site.posts(lang: locale).find_by(title: @site.licencia.name)
      end
    end
  end

  test 'se pueden cambiar las licencias' do
    name = SecureRandom.hex

    post sites_url, headers: @authorization, params: {
      site: {
        name: name,
        title: name,
        description: name * 2,
        design_id: create(:design).id,
        licencia_id: Licencia.first.id,
        deploys_attributes: {
          '0' => {
            type: 'DeployLocal'
          }
        }
      }
    }

    site = Site.find_by_name(name)

    put site_url(site), headers: @authorization, params: {
      site: { licencia_id: Licencia.all.second.id }
    }

    site.reload

    I18n.available_locales.each do |locale|
      Mobility.with_locale(locale) do
        assert site.posts(lang: locale).find_by(title: site.licencia.name)
      end
    end

    site.destroy
  end
end
