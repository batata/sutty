# frozen_string_literal: true

# Controlador de traducciones
class I18nController < ApplicationController
  include Pundit
  before_action :authenticate_usuarie!

  def index
    @site = find_site
    authorize SiteTranslation.new(@site)

    redirect_to site_i18n_edit_path(@site)
  end

  def edit
    @site = find_site
    authorize SiteTranslation.new(@site)

    @lang_from = params.fetch(:from, I18n.locale.to_s)
    @lang_to = params.fetch(:to, @lang_from)
    @options = I18n.available_locales.map do |lang|
      [t("i18n.#{lang}"), lang.to_s]
    end
  end

  def update
    @site = find_site
    authorize SiteTranslation.new(@site)

    @lang_to = params.require(:i18n).require(:lang_to)
    # No usamos params porque nos obliga a hacer una lista blanca de
    # todos los parámetros que queremos, pero no tenemos forma aun de
    # pasarse a permit un array de todas las keys y sus tipos en base al
    # idioma que ya existe
    i = JekyllI18n.new(site: @site,
                       lang: @lang_to,
                       attributes: request.parameters[:i18n][@lang_to])

    if i.save
      redirect_to site_path(@site)
    else
      render 'i18n/edit'
    end
  end
end
