document.addEventListener('turbolinks:load', () => {
  // Al enviar el formulario del artículo, aplicar la validación
  // localmente y actualizar los comentarios para lectores de pantalla.
  document.querySelectorAll('form').forEach(form => {
    form.addEventListener('submit', event => {
      const invalid_help = form.querySelectorAll('.invalid-help')
      const sending_help = form.querySelectorAll('.sending-help')

      invalid_help.forEach(i => i.classList.add('d-none'))
      sending_help.forEach(i => i.classList.add('d-none'))

      form.querySelectorAll('[aria-invalid="true"]').forEach(aria => {
        aria.setAttribute('aria-invalid', false)
        aria.setAttribute('aria-describedby', aria.parentElement.querySelector('.feedback').id)
      })

      if (form.checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()

        invalid_help.forEach(i => i.classList.remove('d-none'))

        form.querySelectorAll(':invalid').forEach(invalid => {
          invalid.setAttribute('aria-invalid', true)
          invalid.setAttribute('aria-describedby', invalid.parentElement.querySelector('.invalid-feedback').id)
        })
      } else {
        sending_help.forEach(i => i.classList.remove('d-none'))
      }

      form.classList.add('was-validated')
    })
  })
})
