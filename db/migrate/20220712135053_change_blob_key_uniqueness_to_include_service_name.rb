# frozen_string_literal: true

# Cambia el índice único para incluir el nombre del servicio, de forma
# que podamos tener varias copias del mismo sitio (por ejemplo para
# test) sin que falle la creación de archivos.
class ChangeBlobKeyUniquenessToIncludeServiceName < ActiveRecord::Migration[6.1]
  def change
    remove_index :active_storage_blobs, %i[key], unique: true
    add_index :active_storage_blobs, %i[key service_name], unique: true
  end
end
