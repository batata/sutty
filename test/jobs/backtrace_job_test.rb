# frozen_string_literal: true

require 'test_helper'

class DeployJobTest < ActiveSupport::TestCase
  def site
    @site ||= create :site
  end

  # Mockup
  def job
    job = BacktraceJob.new
    job.instance_variable_set :@site, site
    job.instance_variable_set :@params, notice

    job
  end

  # setTimeout(() => { throw('Prueba') }, 1000)
  def notice
    @notice ||= {
      'errors' => [
        {
          'type' => '',
          'message' => 'Prueba',
          'backtrace' => [
            {
              'function' => 'pt</e.prototype.notify',
              'file' => 'https://tintalimon.com.ar/assets/js/pack.js',
              'line' => 89,
              'column' => 74_094
            },
            {
              'function' => 'pt</e.prototype.onerror',
              'file' => 'https://tintalimon.com.ar/assets/js/pack.js',
              'line' => 89,
              'column' => 74_731
            },
            {
              'function' => 'pt</e.prototype._instrument/window.onerror',
              'file' => 'https://tintalimon.com.ar/assets/js/pack.js',
              'line' => 89,
              'column' => 71_925
            },
            {
              'function' => 'setTimeout handler*',
              'file' => 'debugger eval code',
              'line' => 1,
              'column' => 11
            }
          ]
        }
      ],
      'context' => {
        'severity' => 'error',
        'history' => [
          {
            'type' => 'error',
            'target' => 'html. > head. > script.[type="text/javascript"][src="//stats.habitapp.org/piwik.js"]',
            'date' => '2021-04-26T22:06:58.390Z'
          },
          {
            'type' => 'DOMContentLoaded',
            'target' => '[object HTMLDocument]',
            'date' => '2021-04-26T22:06:58.510Z'
          },
          {
            'type' => 'load',
            'target' => '[object HTMLDocument]',
            'date' => '2021-04-26T22:06:58.845Z'
          },
          {
            'type' => 'xhr',
            'date' => '2021-04-26T22:06:58.343Z',
            'method' => 'GET',
            'url' => 'assets/data/site.json',
            'statusCode' => 200,
            'duration' => 506
          },
          {
            'type' => 'xhr',
            'date' => '2021-04-26T22:06:58.886Z',
            'method' => 'GET',
            'url' => 'assets/templates/cart.html',
            'statusCode' => 200,
            'duration' => 591
          }
        ],
        'windowError' => true,
        'notifier' => {
          'name' => 'airbrake-js/browser',
          'version' => '1.4.2',
          'url' => 'https://github.com/airbrake/airbrake-js/tree/master/packages/browser'
        },
        'userAgent' => 'Mozilla/5.0 (Windows NT 6.1; rv:85.0) Gecko/20100101 Firefox/85.0',
        'url' => 'https://tintalimon.com.ar/carrito/',
        'rootDirectory' => 'https://tintalimon.com.ar',
        'language' => 'JavaScript'
      },
      'params' => {},
      'environment' => {},
      'session' => {}
    }

    # XXX: Siempre devolvemos un duplicado porque BacktraceJob lo
    # modifica
    @notice.dup
  end

  # Asegurarse que el sitio se destruye al terminar de usarlo
  teardown do
    site&.destroy
  end

  test 'al recibir un backtrace enviamos un error' do
    ActionMailer::Base.deliveries.clear

    assert BacktraceJob.perform_now site_id: site.id, params: notice

    email = ActionMailer::Base.deliveries.first

    assert email
    assert_equal ' (BacktraceJob::BacktraceException) "tintalimon.com.ar: Prueba"', email.subject
    assert(%r{webpack://} =~ email.body.to_s)
  end

  test 'los errores se basan en un sitio' do
    assert_equal site, job.send(:site)
  end

  test 'los errores tienen archivos fuente' do
    assert_equal %w[https://tintalimon.com.ar/assets/js/pack.js], job.send(:sources)
  end

  test 'los errores tienen una url de origen' do
    assert_equal 'tintalimon.com.ar', job.send(:origin)
  end

  test 'los errores tienen un sourcemap' do
    local_job = job
    sourcemap = local_job.send :sourcemap

    assert_equal SourceMap::Map, sourcemap.class
    assert_equal 'assets/js/pack.js', sourcemap.filename
    assert sourcemap.sources.size.positive?
  end
end
