# frozen_string_literal: true

Rails.application.configure do
  next if Rails.env.test?

  domain = ENV.fetch('SUTTY', 'sutty.nl')

  config.hosts << "panel.#{domain}"
  config.hosts << "api.#{domain}"
  config.hosts << /\Aapi\./
end
