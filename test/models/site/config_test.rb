# frozen_string_literal: true

class ConfigText < ActiveSupport::TestCase
  setup do
    @rol = create :rol
    @site = @rol.site
    @usuarie = @rol.usuarie
  end

  teardown do
    @site.destroy
  end

  test 'se puede leer' do
    assert @site.config.is_a?(Site::Config)
    assert_equal @site, @site.config.site
    assert @site.config.plugins.count.positive?
  end

  test 'se puede escribir' do
    assert_nothing_raised do
      @site.config.name = 'Test'
      @site.config.lang = 'es'
    end

    assert @site.config.write

    config = Site::Config.new(@site)

    assert_equal 'Test', config.name
    assert_equal 'es', config.lang
  end

  test 'se puede obtener información' do
    assert @site.config.fetch('noexiste', true)
    assert @site.config.fetch('sass', false)
  end
end
