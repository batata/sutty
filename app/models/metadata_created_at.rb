# frozen_string_literal: true

# Fecha y hora de creación
class MetadataCreatedAt < MetadataTemplate
  # Por defecto la hora actual, pero por retrocompatibilidad, queremos
  # la fecha de publicación
  def default_value
    if post.date.value.to_date < Time.now.to_date
      post.date.value
    else
      Time.now
    end
  end

  # Nunca cambia
  def value=(new_value)
    value
  end
end
