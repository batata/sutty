# frozen_string_literal: true

# Realiza tareas de limpieza en todos los sitios, para optimizar y
# liberar espacio.
class CleanupService
  # Días de antigüedad de los sitios
  attr_reader :before

  # @param :before [ActiveSupport::TimeWithZone] Cuánto tiempo lleva sin usarse un sitio.
  def initialize(before: 30.days.ago)
    @before = before
  end

  # Limpieza general
  #
  # @return [nil]
  def cleanup_everything!
    cleanup_older_sites!
    cleanup_newer_sites!
  end

  # Encuentra todos los sitios sin actualizar y realiza limpieza.
  #
  # @return [nil]
  def cleanup_older_sites!
    Site.where('updated_at < ?', before).order(updated_at: :desc).find_each do |site|
      next unless File.directory? site.path

      Rails.logger.info "Limpiando dependencias, archivos temporales y repositorio git de #{site.name}"

      site.deploys.find_each(&:cleanup!)

      site.repository.gc
      site.repository.lfs_cleanup
      site.touch
    end
  end

  # Tareas para los sitios en uso
  #
  # @return [nil]
  def cleanup_newer_sites!
    Site.where('updated_at >= ?', before).order(updated_at: :desc).find_each do |site|
      next unless File.directory? site.path

      Rails.logger.info "Limpiando repositorio git de #{site.name}"

      site.repository.gc
      site.repository.lfs_cleanup
      site.touch
    end
  end
end
