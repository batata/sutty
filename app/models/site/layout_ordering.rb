# frozen_string_literal: true

class Site
  # Obtiene un listado de layouts (schemas)
  module LayoutOrdering
    extend ActiveSupport::Concern

    included do

      # Obtiene o genera un listado de layouts (schemas) con sus
      # dependencias, para poder generar un árbol.
      #
      # Por defecto, si el sitio no lo soporta, se obtienen los layouts
      # ordenados alfabéticamente por traducción.
      # 
      # @return [Hash]
      def schema_organization
        @schema_organization ||=
          begin
            schema_organization = data.dig('schema', 'organization')
            schema_organization&.symbolize_keys!
            schema_organization&.transform_values! do |ary|
              ary.map(&:to_sym)
            end

            schema_organization ||
              begin
                layouts = self.layouts.sort_by(&:humanized_name).map(&:name)
                Hash[layouts.zip([].fill([], 0, layouts.size))]
              end
          end
      end

      # TODO: Deprecar cuando renombremos layouts a schemas
      alias layout_organization schema_organization
    end
  end
end
