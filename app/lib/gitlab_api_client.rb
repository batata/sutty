# frozen_string_literal: true

require 'httparty'

class GitlabApiClient
  include HTTParty

  # TODO: Hacer configurable por sitio
  base_uri ENV.fetch('GITLAB_URI', 'https://0xacab.org')
  # No seguir redirecciones.  Si nos olvidamos https:// en la dirección,
  # las redirecciones nos pueden llevar a cualquier lado y obtener
  # resultados diferentes.
  no_follow true

  # Trae todos los proyectos.  Como estamos usando un Project Token,
  # siempre va a traer uno solo.
  #
  # @return [HTTParty::Response]
  def projects
    self.class.get('/api/v4/projects', { query: { membership: true }, headers: headers })
  end

  # Obtiene el identificador del proyecto
  #
  # @return [Integer]
  def project_id
    @project_id ||= ENV['GITLAB_PROJECT'] || projects&.first&.dig('id')
  end

  # Crea un issue
  #
  # @see https://docs.gitlab.com/ee/api/issues.html#new-issue
  # @return [HTTParty::Response]
  def new_issue(**args)
    self.class.post("/api/v4/projects/#{project_id}/issues", { body: args, headers: headers })
  end

  # Modifica un issue
  #
  # @see https://docs.gitlab.com/ee/api/issues.html#edit-issue
  # @return [HTTParty::Response]
  def edit_issue(iid:, **args)
    self.class.put("/api/v4/projects/#{project_id}/issues/#{iid}", { body: args, headers: headers })
  end

  # Crea un comentario
  #
  # @see https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
  # @return [HTTParty::Response]
  def new_note(iid:, **args)
    self.class.post("/api/v4/projects/#{project_id}/issues/#{iid}/notes", { body: args, headers: headers })
  end

  private

  def headers(extra = {})
    { 'Authorization' => "Bearer #{ENV['GITLAB_TOKEN']}" }.merge(extra)
  end
end
