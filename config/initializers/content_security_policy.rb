# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Define an application-wide content security policy
# For further information see the following documentation
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy

Rails.application.config.content_security_policy do |policy|
  policy.default_src :self, :blob
  # XXX: Varios scripts generan estilos en línea
  policy.style_src   :self, :unsafe_inline, :https
  # Repetimos la default para poder saber cuál es la política en falta
  policy.script_src  :self
  policy.font_src    :self, :https
  # XXX: Los íconos de Trix se cargan vía data:
  policy.img_src     :self, :data, :https, :blob
  # Ya no usamos applets!
  policy.object_src  :none
  # webpack-dev-server
  if Rails.env.development?
    policy.connect_src :self,
                       "https://#{ENV.fetch('SUTTY', 'sutty.local')}:3035",
                       "wss://#{ENV.fetch('SUTTY', 'sutty.local')}:3035"
  end

  # Specify URI for violation reports
  policy.report_uri "https://api.#{ENV.fetch('SUTTY_WITH_PORT', 'sutty.nl')}/v1/csp_reports.json"
end

# If you are using UJS then enable automatic nonce generation
# Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }

# Set the nonce only to specific directives
# Rails.application.config.content_security_policy_nonce_directives = %w(script-src)

# Report CSP violations to a specified URI
# For further information see the following documentation:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
Rails.application.config.content_security_policy_report_only = false
