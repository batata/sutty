# frozen_string_literal: true

# = Follow =
#
# Una actividad de seguimiento se refiere siempre a une actore (el
# sitio) y proviene de otre actore.
#
# Por ahora las solicitudes de seguimiento se auto-aprueban.
class ActivityPub
  class Activity
    class Follow < ActivityPub::Activity
      # Auto-aprobar la solicitud de seguimiento
      def update_activity_pub_state!
        activity_pub.approve! if activity_pub.may_approve?
      end
    end
  end
end
