# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class CSPReportsControllerTest < ActionDispatch::IntegrationTest
      test 'se puede enviar un reporte' do
        skip

        post v1_csp_reports_url,
             host: "api.#{Site.domain}",
             params: {
               'csp-report': {
                 'document-uri': 'http://example.com/signup.html',
                 referrer: '',
                 'blocked-uri': 'http://example.com/css/style.css',
                 'violated-directive': 'style-src cdn.example.com',
                 'original-policy': "default-src 'none'; style-src cdn.example.com; report-uri /_/csp-reports"
               }
             }

        assert_equal 201, response.status
        assert_equal 1, CspReport.all.count
      end
    end
  end
end
