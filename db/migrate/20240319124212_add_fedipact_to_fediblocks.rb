# frozen_string_literal: true

# Agrega threads.net a las listas de bloqueo
class AddFedipactToFediblocks < ActiveRecord::Migration[6.1]
  def up
    change_column :activity_pub_fediblocks, :download_url, :string, null: true

    fedipact =
      ActivityPub::Fediblock.create(
        hostnames: %w[threads.net],
        title: 'Fedipact',
        url: 'https://fedipact.online/',
        format: 'none'
      )

    DeploySocialDistributedPress.find_each do |deploy|
      FediblockState.create(site: deploy.site, fediblock: fedipact, aasm_state: 'disabled').tap(&:enable!)
    end
  end

  def down
    fedipact = ActivityPub::Fediblock.find_by(url: 'https://fedipact.online/').delete
    FediblockState.where(fediblock_id: fedipact.id).delete_all
    change_column :activity_pub_fediblocks, :download_url, :string, null: false
  end
end
