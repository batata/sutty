# frozen_string_literal: true

# Solo les usuaries pueden moderar comentarios
ActivityPubPolicy = Struct.new(:usuarie, :activity_pub) do
  ActivityPub.events.each do |event|
    define_method(:"#{event}?") do
      activity_pub.site.usuarie? usuarie
    end
  end

  # En este paso tenemos varias instancias por moderar pero todas son
  # del mismo sitio.
  def action_on_several?
    activity_pub.first.site.usuarie? usuarie
  end
end
