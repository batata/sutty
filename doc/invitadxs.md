# Gestión de usuarixs invitadxs

Lxs usuarixs invitadxs solo tienen cuenta en Sutty, no comparten cuenta
con el resto del sistema (Sutty está integrada a otras cuentas vía
IMAP).

Pueden cambiar su contraseña y recuperarla.

Cuando se loguean, solo pueden ver sus artículos y editarlos.  También
pueden crear nuevos.

Cuando crean un artículo o cuando lo editan, los artículos pasan a
estado borrador.  Solo las usuarias de Sutty pueden revisar el artículo
y publicarlo.

Cada invitadx está asociadx a uno o más sitios.


## Idea

Que cada sitio gestione sus propias cuentas usando un archivo en formato
`/etc/passwd` (ver `man 5 passwd`).

Con esto habría portabilidad de cuentas junto con los sitios, pero sería
un problema para poder gestionar varios sitios con cuentas compartidas.

## Temporal

Usar `devise` con una base de datos SQLite.  La idea es descartarla más
adelante y tener un `passwd` + `shadow` de Sutty.  Sino vamos a empezar
a poner cosas en una base de datos y no es la idea...

**No se puede usar devise porque toma el control de toda la gestión de
usuarias.**

## Implementación

Para poder separar la autenticación de usuarias de invitadxs, cada
controlador tiene su propio `namespace`, de forma que no se crucen
funcionalidades.

Lxs invitadxs se almacenan con email y contraseña en una base de datos
SQLite3.

La pertenencia a un sitio se almacena en el archivo `.invitadxs` de cada
sitio (como `.usuarias`).

Además, se vincula el sitio al directorio de invitadx para poder tener
acceso a varios sitios.

El directorio de lx invitadx es `_invitadxs/direccion@mail`.

El registro de cuentas se hace en base al sitio.  Las cuentas son unicas
pero Sutty todavía no soporta asociarse a varixs sitios.

Lxs invitadxs solo pueden ver sus articulos y no pueden cambiar la
autoria de los articulos.

Todo lo que guardan se convierte en borrador.

La configuración de cada sitio tiene que tener los siguientes valores:

```yaml
# Este sitio acepta invitadxs
invitadxs: true
# Configuración del panel de registro
welcome:
  message: Bienvenidxs
  button: '#D53F77'
# Mensaje de agradecimiento al guardar un artículo
thanks: ¡Gracias!
```
