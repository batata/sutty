# frozen_string_literal: true

# Registra el estado de las compilaciones
class AddStatusToBuildStats < ActiveRecord::Migration[6.1]
  def change
    add_column :build_stats, :status, :boolean, default: false
  end
end
