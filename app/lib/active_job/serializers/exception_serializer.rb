# frozen_string_literal: true

require 'json/add/exception'

module ActiveJob
  module Serializers
    class ExceptionSerializer < ObjectSerializer # :nodoc:
      def serialize(ex)
        super('value' => { 'class' => ex.class.name, 'exception' => ex.as_json })
      end

      def deserialize(hash)
        hash.dig('value', 'class').constantize.json_create(hash.dig('value', 'exception'))
      end

      private
        def klass
          Exception
        end
    end
  end
end
