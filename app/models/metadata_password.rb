# frozen_string_literal: true

# Almacena una contraseña
class MetadataPassword < MetadataString
  # Las contraseñas no son indexables
  #
  # @return [boolean]
  def indexable?
    false
  end

  private

  alias_method :original_sanitize, :sanitize

  # Sanitizar la string y generar un hash Bcrypt
  #
  # @param :string [String]
  # @return [String]
  def sanitize(string)
    string = original_sanitize string

    ::BCrypt::Password.create(string).to_s
  end
end
