# frozen_string_literal: true

module ModerationFiltersConcern
  extend ActiveSupport::Concern

  included do
    before_action :store_filters_in_session!, only: %i[index show]

    private

    def store_filters_in_session!
      session[:moderation_queue_filters] = params.permit(:instance_state, :actor_state, :activity_pub_state)
    end
  end
end
