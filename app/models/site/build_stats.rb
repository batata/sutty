# frozen_string_literal: true

class Site
  module BuildStats
    extend ActiveSupport::Concern

    included do
      # Devuelve el tiempo promedio de publicación para este sitio
      #
      # @return [Integer]
      def average_publication_time
        build_stats.group(:action).average(:seconds).values.reduce(:+).round
      end

      # Devuelve el tiempo promedio de compilación para sitios similares
      # a este.
      #
      # @return [Integer]
      def average_publication_time_for_similar_sites
        similar_deploys = Deploy.where(type: deploys.pluck(:type)).pluck(:id)

        BuildStat.where(deploy_id: similar_deploys).group(:action).average(:seconds).values.reduce(:+).round
      end

      # Define si podemos calcular el tiempo promedio de publicación
      # para este sitio
      #
      # @return [Boolean]
      def average_publication_time_calculable?
        build_stats.jekyll.where(status: true).count > 1
      end

      def similar_sites?
        !design.no_theme?
      end

      # Detecta si el sitio todavía no ha sido publicado
      #
      # @return [Boolean]
      def not_published_yet?
        build_stats.jekyll.where(status: true).count.zero?
      end

      # Cambios posibles luego de la última publicación exitosa:
      #
      # * Artículos modificados
      # * Configuración modificada
      # * Métodos de publicación añadidos
      #
      # @return [Boolean]
      def awaiting_publication?
        waiting? && (post_pending? || deploy_pending? || configuration_pending?)
      end

      # Se modificaron artículos después de publicar el sitio por última
      # vez
      #
      # @return [Boolean]
      def post_pending?
        last_indexed_post_time > last_publication_time
      end

      # Se modificó el sitio después de publicarlo por última vez
      #
      # @return [Boolean]
      def deploy_pending?
        last_deploy_time > last_publication_time
      end

      # Se modificó la configuración del sitio
      #
      # @return [Boolean]
      def configuration_pending?
        last_configuration_time > last_publication_time
      end

      private

      # Encuentra la fecha del último artículo modificado.  Si no hay
      # ninguno, devuelve la fecha de modificación del sitio.
      #
      # @return [Time]
      def last_indexed_post_time
        indexed_posts.order(updated_at: :desc).select(:updated_at).first&.updated_at || updated_at
      end

      # Encuentra la fecha de última modificación de los métodos de
      # publicación.
      #
      # @return [Time]
      def last_deploy_time
        deploys.order(created_at: :desc).select(:created_at).first&.created_at || updated_at
      end

      # Encuentra la fecha de última publicación exitosa, si no hay
      # ninguno, devuelve la fecha de modificación del sitio.
      #
      # @return [Time]
      def last_publication_time
        build_stats.jekyll.where(status: true).order(created_at: :desc).select(:created_at).first&.created_at || updated_at
      end

      # Fecha de última modificación de la configuración
      #
      # @return [Time]
      def last_configuration_time
        File.mtime(config.path)
      end
    end
  end
end
