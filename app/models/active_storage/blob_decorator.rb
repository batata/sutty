# frozen_string_literal: true

module ActiveStorage
  # Modificaciones a ActiveStorage::Blob
  module BlobDecorator
    extend ActiveSupport::Concern

    included do
      # Permitir que llegue el nombre de archivo al servicio de subida de
      # archivos.
      #
      # @return [Hash]
      def service_metadata
        if forcibly_serve_as_binary?
          { content_type: ActiveStorage.binary_content_type, disposition: :attachment, filename: filename }
        elsif !allowed_inline?
          { content_type: content_type, disposition: :attachment, filename: filename }
        else
          { content_type: content_type, filename: filename }
        end
      end
    end
  end
end

ActiveStorage::Blob.include ActiveStorage::BlobDecorator
