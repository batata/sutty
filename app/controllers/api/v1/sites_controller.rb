# frozen_string_literal: true

module Api
  module V1
    # API para sitios
    class SitesController < BaseController
      http_basic_authenticate_with name: ENV['HTTP_BASIC_USER'],
                                   password: ENV['HTTP_BASIC_PASSWORD']

      # Lista de nombres de dominios a emitir certificados
      def index
        render json: alternative_names + api_names + www_names
      end

      private

      def canonicalize(name)
        name.end_with?('.') ? name[0..-2] : "#{name}.#{Site.domain}"
      end

      def subdomain?(name)
        name.end_with? ".#{Site.domain}"
      end

      # Dominios alternativos
      def alternative_names
        (DeployAlternativeDomain.all.map(&:hostname) + DeployLocalizedDomain.all.map(&:hostname)).map do |name|
          canonicalize name
        end.reject do |name|
          subdomain? name
        end
      end

      # Obtener todos los sitios con API habilitada, es decir formulario
      # de contacto y/o colaboración anónima.
      #
      # TODO: Optimizar
      def api_names
        Site.where(contact: true)
            .or(Site.where(colaboracion_anonima: true))
            .select("'api.' || name as name").map(&:name).map do |name|
              canonicalize name
            end.reject do |name|
              subdomain? name
            end
      end

      # Todos los dominios con WWW habilitado
      def www_names
        Site.where(id: DeployWww.all.pluck(:site_id)).select("'www.' || name as name").map(&:name).map do |name|
          canonicalize name
        end
      end
    end
  end
end
