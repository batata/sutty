# frozen_string_literal: true

# Permite generar el sitio en una versión privada, mostrando información
# que no se vería públicamente (borradores, campos privados, etc.)
#
# XXX: La plantilla tiene que soportar esto con el plugin
# jekyll-private-data
class DeployPrivate < DeployLocal
  DEPENDENCIES = %i[deploy_local]

  # No es necesario volver a instalar dependencias
  def deploy(output: false)
    jekyll_build(output: output)
  end

  # Hacer el deploy a un directorio privado
  def destination
    File.join(Rails.root, '_private', site.name)
  end

  def url
    "#{ENV['PANEL_URL']}/sites/private/#{site.name}"
  end

  # No usar recursos en compresión y habilitar los datos privados
  def env
    @env ||= super.merge({
                           'JEKYLL_ENV' => 'development',
                           'JEKYLL_PRIVATE' => site.private_key
                         })
  end
end
