# frozen_string_literal: true

require 'test_helper'

class MetadataBooleanTest < ActiveSupport::TestCase
  test 'son false por defecto' do
    site = create :site
    post = site.posts.build

    assert_equal false, post.draft.value

    site.destroy
  end

  test 'pueden ser 0' do
    site = create :site
    post = site.posts.build

    post.draft.value = '0'

    assert_equal false, post.draft.value

    post.save

    assert_equal false, post.draft.value

    site.destroy
  end

  test 'pueden ser 1' do
    site = create :site
    post = site.posts.build

    post.draft.value = '1'

    assert_equal true, post.draft.value

    post.save

    assert_equal true, post.draft.value

    site.destroy
  end

  test 'pueden ser false' do
    site = create :site
    post = site.posts.build

    post.draft.value = false

    assert_equal false, post.draft.value

    post.save

    assert_equal false, post.draft.value

    site.destroy
  end

  test 'pueden ser true' do
    site = create :site
    post = site.posts.build

    post.draft.value = true

    assert_equal true, post.draft.value

    post.save

    assert_equal true, post.draft.value

    site.destroy
  end
end
