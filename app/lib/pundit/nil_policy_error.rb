# frozen_string_literal: true

# La excepción para capturar un recurso no encontrado y mostrar un 404
module Pundit
  class NilPolicyError < RuntimeError; end
end
