# Editor de texto

Tenemos varias posibilidades para editores de texto:

# [Trix](https://trix-editor.org/)

Es el editor WYSIWYG integrado a Rails 6.  Es simple, liviano, de
relativa fácil extensibilidad, se pueden subir archivos directamente,
pero:

* No soporta markdown, con lo que hay que convertir de HTML a MD
  internamente

* Hay que agregarle botones para H2-H6 porque solo tiene H1

* No soporta tablas, aunque alguien lo logró hacer si hiciera falta.
  (Podemos abrir un editor de ethercalc o simil quizas y tomar los datos
  importándolos en HTML).

# [Codemirror](https://codemirror.net/)

Es un editor de texto con resaltado de sintaxis, pero:

* Como no es WYSIWYG, la gente se puede asustar.  Solo serviría para
  quienes se animan a (o quieren) escribir directamente con MD

* Habría que desarrollar una vista previa

* Habría que hacer una interfaz para vincular imágenes y otras cosas

# [ACE](https://ace.c9.io/)

Es un editor de texto como Codemirror aunque con más opciones y más
liviano (?), pero:

* Ídem codemirror

# [TUI-Editor](https://ui.toast.com/tui-editor/)

Es un editor WYSIWYG para Markdown, soporta tablas y un montón de
features, pero:

* Está hecho en jQuery
* El JS es gigante y es super complejo, con lo que no podríamos
  extenderlo si hiciera falta
* No modifica un textarea con lo que no hay relación inmediata entre el
  texto escrito y lo que se envía luego, puede haber bugs donde no
  mandemos o no guardemos todo el texto.
* Tiene analytics incorporadas aunque se pueden deshabilitar

# Editor para Sutty

Por ahora vamos a ir con Trix ya que es el que más características
básicas tiene por la cantidad de esfuerzo que insume.  Nos interesa dar
la posibilidad de una edición más hacker (!), así que quizás integremos
ACE o Codemirror más adelante.

# Arquitectura

Todos los campos de metadatos de tipo `content` se van a convertir en
editores Trix.

Trix envía HTML, que se convierte a Markdown con reverse_markdown.  Al
cargar el texto para editar, hay que volver a convertirlo a HTML para
que Trix lo entienda.  No tiene que haber incompatibilidades porque en
cada edición se van a degradar los textos sino.

# TODO

* Crear plugin que convierta imagenes remotas en locales
* Probar qué otros adjuntos soporta trix y limitarlos (?)
* Agregar soporte de notas al pie en reverse_markdown

# Futuro

Finalmente Trix no es el editor adecuado porque genera una entrada
desprolija con `<div>` y `<br><br>` en lugar de párrafos, con lo que el
markdown generado tiene que ser limpiado para poder salir como html.

Estamos investigando [ProseMirror](http://prosemirror.net/) como
reemplazo.  Es un poco más de trabajo, pero podríamos tener:

* [CommonMark](http://prosemirror.net/examples/markdown/)
* [Subida directa de archivos](http://prosemirror.net/examples/upload/)
* [Tablas](https://github.com/ProseMirror/prosemirror-tables)

En lugar de tener que modificar Trix...

Además, queremos permitir edición de texto con Ace/Codemirror, para
usuaries no WYSIWY{G,M}.
