# frozen_string_literal: true

# Una lista de valores
class MetadataArray < MetadataTemplate
  # El valor por defecto es una array vacía
  def default_value
    super || []
  end

  # Los Arrays no se pueden cifrar todavía
  # TODO: Cifrar y decifrar arrays
  def private?
    false
  end

  # Solo los datos públicos se indexan, aunque MetadataArray no se cifra
  # aun, dejamos esto preparado para la posteridad.
  def indexable?
    true && !private?
  end

  def to_s
    value.join(', ')
  end

  # Obtiene el valor desde el documento, convirtiéndolo a Array si no lo
  # era ya, por retrocompabilidad.
  #
  # @return [Array]
  def document_value
    [super].flatten(1).compact
  end

  alias indexable_values value

  private

  # TODO: Sanitizar otros valores
  # XXX: Por qué eliminamos el punto del final?
  def sanitize(values)
    values.map do |v|
      case v
      when String then super(v).sub(/\.\z/, '')
      else v
      end
    end.select(&:present?)
  end
end
