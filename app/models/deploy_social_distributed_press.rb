# frozen_string_literal: true

require 'distributed_press/v1/social/client'

# Publicar novedades al Fediverso
class DeploySocialDistributedPress < Deploy
  # Solo luego de publicar remotamente
  DEPENDENCIES = %i[deploy_distributed_press deploy_rsync deploy_full_rsync].freeze

  # Envía las notificaciones
  def deploy(output: false)
    with_tempfile(site.private_key_pem) do |file|
      key = Shellwords.escape file.path
      dest = Shellwords.escape destination

      run(%(bundle exec jekyll notify --trace --key #{key} --destination "#{dest}"), output: output).tap do |_|
        create_hooks!
        enable_fediblocks!
      end
    end
  end

  # Igual que DeployLocal
  #
  # @return [String]
  def destination
    File.join(Rails.root, '_deploy', site.hostname)
  end

  # Solo uno
  #
  # @return [Integer]
  def limit
    1
  end

  # Espacio ocupado, pero no podemos calcularlo
  #
  # @return [Integer]
  def size
    0
  end

  # El perfil de actor
  #
  # @return [String,nil]
  def url
    site.data.dig('activity_pub', 'actor')
  end

  # Genera la opción de llave privada para jekyll build
  #
  # @params :args [Hash]
  # @return [String]
  def flags_for_build(**args)
    "--key #{Shellwords.escape args[:private_key].path}"
  end

  private

  # Crea los hooks en la Social Inbox para que nos avise de actividades
  # nuevas
  #
  # @return [nil]
  def create_hooks!
    hook_client = site.social_inbox.hook
    webhook_class = DistributedPress::V1::Social::Schemas::Webhook

    hook_client.class::EVENTS.each do |event|
      event_url = :"v1_site_webhooks_#{event}_url"

      webhook =
        webhook_class.new.call({
                                 method: 'POST',
                                 url: Rails.application.routes.url_helpers.public_send(
                                   event_url, site_id: site.name, host: site.social_inbox_hostname
                                 ),
                                 headers: {
                                   'X-Social-Inbox': rol.token
                                 }
                               })

      raise ArgumentError, webhook.errors.messages if webhook.failure?

      response = hook_client.put(event: event, hook: webhook)

      raise ArgumentError, response.body unless response.success?
    rescue ArgumentError => e
      ExceptionNotifier.notify_exception(e, data: { site_id: site.name, usuarie_id: rol.usuarie_id })
    end
  end

  # Habilita todos los fediblocks disponibles.
  #
  # @todo Hacer que algunos sean opcionales
  # @todo Mover a un Job
  def enable_fediblocks!
    ActivityPub::Fediblock.find_each do |fediblock|
      site.fediblock_states.find_or_create_by(fediblock: fediblock).tap do |state|
        state.enable! if state.may_enable?
      end
    end
  end
end
