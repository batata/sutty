# frozen_string_literal: true

# Asigna un identificador único al artículo
class MetadataUuid < MetadataTemplate
  def default_value
    SecureRandom.uuid
  end

  def private?
    false
  end
end
