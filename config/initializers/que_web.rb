# frozen_string_literal: true

Que::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [ENV.fetch('HTTP_BASIC_USER', nil), ENV.fetch('HTTP_BASIC_PASSWORD', nil)]
end
