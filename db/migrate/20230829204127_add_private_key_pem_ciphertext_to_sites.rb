# frozen_string_literal: true

# Almacena las llaves privadas de cada sitio
class AddPrivateKeyPemCiphertextToSites < ActiveRecord::Migration[6.1]
  # Agrega la columna cifrada
  def change
    add_column :sites, :private_key_pem_ciphertext, :text
  end
end
