# frozen_string_literal: true

# Agregar créditos y diseñadorxs
class AddCreditsToDesigns < ActiveRecord::Migration[6.0]
  def change
    add_column :designs, :credits, :text
    add_column :designs, :designer_url, :string
  end
end
