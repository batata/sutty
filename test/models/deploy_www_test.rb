# frozen_string_literal: true

require 'test_helper'

class DeployWwwTest < ActiveSupport::TestCase
  test 'se puede deployear' do
    site = create :site
    local = create :deploy_local, site: site
    deploy = create :deploy_www, site: site

    # Primero tenemos que generar el sitio
    local.deploy

    assert deploy.deploy
    assert File.symlink?(deploy.destination)

    local.destroy
  end
end
