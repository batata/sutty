# frozen_string_literal: true

# Una instancia es la instancia por defecto
class AddDefaultToDistributedPressPublisher < ActiveRecord::Migration[6.1]
  def up
    add_column :distributed_press_publishers, :default, :boolean, default: false

    DistributedPressPublisher.last&.update(default: true)
  end

  def down
    remove_column :distributed_press_publishers, :default
  end
end
