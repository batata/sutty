# frozen_string_literal: true

# Gestiona la cola de moderación de actores
class ActorModerationsController < ApplicationController
  include ModerationConcern
  include ModerationFiltersConcern

  before_action :authenticate_usuarie!

  breadcrumb -> { current_usuarie.email }, :edit_usuarie_registration_path
  breadcrumb 'sites.index', :sites_path, match: :exact

  ActorModeration.events.each do |actor_event|
    define_method(actor_event) do
      authorize actor_moderation

      # Crea una RemoteFlag si se envían los parámetros adecuados
      if actor_event == :report
        remote_flag_params(actor_moderation).tap do |p|
          actor_moderation.remote_flag_id = p[:remote_flag_attributes][:id]
          actor_moderation.update(p)
        end
      end

      message =
        if actor_moderation.public_send(:"may_#{actor_event}?") && actor_moderation.public_send(:"#{actor_event}!")
          :success
        else
          :error
        end

      flash[message] = I18n.t("actor_moderations.#{actor_event}.#{message}")

      redirect_to_moderation_queue!
    end
  end

  # Ver el perfil remoto
  def show
    breadcrumb site.title, site_posts_path(site)
    breadcrumb I18n.t('moderation_queue.index.title'), site_moderation_queue_path(site)

    @remote_profile = actor_moderation.actor.content
    @moderation_queue = rubanok_process(site.activity_pubs.where(actor_id: actor_moderation.actor_id),
                                        with: ActivityPubProcessor)

    breadcrumb @remote_profile['name'] || actor_moderation.actor.mention || actor_moderation.actor.uri, ''
  end

  def action_on_several
    redirect_to_moderation_queue!

    actor_moderations = site.actor_moderations.where(id: params[:actor_moderation])

    return if actor_moderations.count.zero?

    authorize actor_moderations

    action = params[:actor_moderation_action].to_sym
    method = :"#{action}_all!"
    may    = :"may_#{action}?"

    return unless ActorModeration.events.include? action

    ActorModeration.transaction do
      if action == :report
        actor_moderations.find_each do |actor_moderation|
          next unless actor_moderation.public_send(may)

          actor_moderation.update(actor_moderation_params(actor_moderation))
        end
      end

      message = actor_moderations.public_send(method) ? :success : :error

      flash[message] = I18n.t("actor_moderations.action_on_several.#{message}")
    end
  end

  private

  def actor_moderation
    @actor_moderation ||= site.actor_moderations.find(params[:actor_moderation_id] || params[:id])
  end
end
