# frozen_string_literal: true

module CoreExtensions
  module Jekyll
    module Document
      # Permite cambiar la ubicación del archivo para que podamos leerlo
      # sin tener que instanciar uno nuevo.
      module Path
        def path=(new_path)
          @path = new_path
          # Dejar que lo recalcule al releer
          @cleaned_relative_path =
            @extname =
              @basename =
                @basename_without_ext =
                  @output_ext =
                    @relative_path = nil
        end
      end
    end
  end
end
