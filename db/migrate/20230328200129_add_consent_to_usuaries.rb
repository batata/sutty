# frozen_string_literal: true

# Agrega consentimientos a les usuaries.  No usamos un loop de
# Usuarie::CONSENT_FIELDS porque quizás agreguemos campos luego.
class AddConsentToUsuaries < ActiveRecord::Migration[6.1]
  def change
    add_column :usuaries, :privacy_policy_accepted_at, :datetime
    add_column :usuaries, :terms_of_service_accepted_at, :datetime
    add_column :usuaries, :code_of_conduct_accepted_at, :datetime
    add_column :usuaries, :available_for_feedback_accepted_at, :datetime
  end
end
