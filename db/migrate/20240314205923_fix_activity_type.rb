# frozen_string_literal: true

# Soportar nuevos tipos
class FixActivityType < ActiveRecord::Migration[6.1]
  def up
    %w[Like Announce].each do |type|
      ActivityPub::Activity.where(Arel.sql("content->>'type' = '#{type}'")).update_all(
        type: "ActivityPub::Activity::#{type}", updated_at: Time.now
      )
    end
  end

  def down; end
end
