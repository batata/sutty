# frozen_string_literal: true

# Una tabla que lleva el recuento de recolección de estadísticas, solo
# es necesario para saber cuándo se hicieron, si se hicieron y usar como
# caché.
class CreateStats < ActiveRecord::Migration[6.1]
  def change
    create_table :stats do |t|
      t.timestamps
    end
  end
end
