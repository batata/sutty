# frozen_string_literal: true

# Una lista de valores predefinidos
class MetadataPredefinedArray < MetadataArray
  def values
    @values ||= layout.dig(:metadata, name, 'values')&.map do |k, v|
      [v[I18n.locale.to_s], k]
    end&.to_h
  end
end
