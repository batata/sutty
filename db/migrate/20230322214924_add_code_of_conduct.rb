# frozen_string_literal: true

# Crea códigos de conducta
class AddCodeOfConduct < ActiveRecord::Migration[6.1]
  def up
    create_table :codes_of_conduct do |t|
      t.timestamps
      t.string :title
      t.text :description
      t.text :content
    end

    # XXX: En lugar de ponerlo en las seeds
    YAML.safe_load(File.read('db/seeds/codes_of_conduct.yml')).each do |coc|
      CodeOfConduct.new(**coc).save!
    end
  end

  def down
    drop_table :codes_of_conduct
  end
end
