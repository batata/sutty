# frozen_string_literal: true

# Crea la tabla de deploys posibles para un sitio
class CreateDeploys < ActiveRecord::Migration[5.2]
  def change
    create_table :deploys do |t|
      t.timestamps
      t.belongs_to :site, index: true
      t.string :type, index: true
      t.text :values
    end
  end
end
