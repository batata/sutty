# frozen_string_literal: true

class Site
  # Gestor de formularios del sitio
  module Forms
    # Esta clase es un Hash que es capaz de convertirse a strong params
    # según la definición del formulario en el sitio.
    class Form < Hash
      # Convierte el formulario a strong params
      #
      # @return Array
      def params
        map do |field, definition|
          if ARRAY_FIELDS.include? definition['type']
            { field.to_sym => [] }
          else
            field.to_sym
          end
        end.compact
      end

      # Obtiene la traducción del campo
      #
      # @return String
      def t(field)
        dig(field.to_s, 'label', I18n.locale.to_s) || field.to_s
      end
    end

    # Campos del formulario que no son necesarios
    EXCLUDED_FIELDS = %w[separator redirect].freeze
    # Campos que aceptan valores múltiples (checkboxes, input-map,
    # input-tag)
    ARRAY_FIELDS = %w[array].freeze

    # Obtiene todos los formularios disponibles
    #
    # @return Array Formularios disponibles para este sitio
    def forms
      @forms ||= data.dig('forms')&.keys || []
    end

    # El nombre del formulario está disponible
    #
    # @param [String|Symbol] El nombre del formulario
    def form?(name)
      forms.include? name.to_s
    end

    # Obtiene un formulario con los campos definitivos
    #
    # @return Site::Forms::Form
    def form(name)
      @cached_forms ||= {}
      @cached_forms[name] ||= Form[data.dig('forms', name).reject { |k, _| excluded? k }]
    end

    # Detecta si el campo está excluido del formulario final
    def excluded?(field)
      EXCLUDED_FIELDS.include? field.to_s
    end
  end
end
