class AddRemoteUserToAccessLogs < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    add_column :access_logs, :remote_user, :string
  end
end
