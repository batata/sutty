# frozen_string_literal: true

require 'httparty'

class HiddenServiceClient
  include HTTParty

  base_uri ENV.fetch('HIDDEN_SERVICE', 'http://tor:3000')

  def create(name)
    self.class.get("/#{name}").body
  end
end
