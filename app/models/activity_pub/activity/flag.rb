# frozen_string_literal: true

class ActivityPub
  class Activity
    class Flag < ActivityPub::Activity; end
  end
end
