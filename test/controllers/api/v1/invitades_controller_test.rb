# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class PostsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @rol = create :rol
        @site = @rol.site
        @usuarie = @rol.usuarie

        @site.update_attribute :colaboracion_anonima, true
        @host = { host: "api.#{Site.domain}" }
      end

      teardown do
        @site.destroy
      end

      test 'primero hay que pedir una cookie' do
        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        assert cookies[@site.name]
        assert cookies['_sutty_session']
      end

      test 'solo si el sitio existe' do
        site = SecureRandom.hex

        get v1_site_invitades_cookie_url(site_id: site, **@host)

        assert_not cookies[site]
        assert_not cookies['_sutty_session']
      end

      test 'solo si el sitio tiene colaboracion anonima' do
        @site.update_attribute :colaboracion_anonima, false

        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        assert_not cookies[@site.name]
        assert_not cookies['_sutty_session']
      end
    end
  end
end
