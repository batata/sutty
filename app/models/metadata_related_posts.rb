# frozen_string_literal: true

# Devuelve una lista de títulos y UUID de todos los posts del mismo
# idioma que el actual, para usar con input-map.js
class MetadataRelatedPosts < MetadataArray
  # Genera un Hash de { title | slug => uuid } y excluye el Post actual
  # @return [Hash]
  def values
    @values ||= posts.map do |p|
      next if p.uuid.value == post.uuid.value

      [title(p), p.uuid.value]
    end.compact.to_h
  end

  # Las relaciones nunca son privadas
  def private?
    false
  end

  def indexable?
    false
  end

  def indexable_values
    posts.where(uuid: value).map(&:title).map(&:value)
  end

  private

  # Obtiene todos los posts y opcionalmente los filtra
  def posts
    site.posts(lang: lang).where(**filter)
  end

  def title(post)
    "#{post&.title&.value || post&.slug&.value} #{post&.date&.value.strftime('%F')} (#{post.layout.humanized_name})"
  end

  # Encuentra el filtro
  def filter
    layout.metadata.dig(name, 'filter')&.to_h&.symbolize_keys || {}
  end

  def sanitize(uuid)
    super(uuid.map do |u|
      u.to_s.gsub(/[^a-f0-9\-]/i, '')
    end)
  end
end
