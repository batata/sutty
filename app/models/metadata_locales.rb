# frozen_string_literal: true

# Los valores de este metadato son artículos en otros idiomas
class MetadataLocales < MetadataHasAndBelongsToMany
  # Todos los valores posibles para cada idioma disponible
  #
  # @return { lang: { title: uuid } }
  def values
    @values ||= site.locales.map do |locale|
      [locale, posts.where(lang: locale).map do |post|
        [title(post), post.uuid.value]
      end.to_h]
    end.to_h
  end

  # Siempre hay una relación inversa
  #
  # @return [True]
  def inverse?
    true
  end

  # El campo inverso se llama igual en el otro post
  #
  # @return [Symbol]
  def inverse
    :locales
  end

  private

  # Obtiene todos los locales distintos a este post
  #
  # @return [Array]
  def other_locales
    site.locales.reject do |locale|
      locale == post.lang.value.to_sym
    end
  end

  # Obtiene todos los posts de los otros locales con el mismo layout
  #
  # @return [PostRelation]
  def posts
    other_locales.map do |locale|
      site.posts(lang: locale).where(layout: post.layout.value)
    end.reduce(&:concat) || PostRelation.new(site: site, lang: 'any')
  end
end
