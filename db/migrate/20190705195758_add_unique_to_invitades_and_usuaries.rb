# frozen_string_literal: true

# Agrega índices únicos a la combinación de Usuarie y Site para no tener
# accesos duplicados.
class AddUniqueToInvitadesAndUsuaries < ActiveRecord::Migration[5.2]
  def change
    %i[invitades_sites sites_usuaries].each do |t|
      remove_index t, :site_id
      remove_index t, :usuarie_id
      add_index t, %i[site_id usuarie_id], unique: true
    end
  end
end
