# frozen_string_literal: true

# La diferencia con MetadataRelatedPosts es que la relación también
# actualiza los Posts remotos.
#
# Localmente tenemos un Array de UUIDs.  Remotamente tenemos una String
# apuntando a un Post, que se mantiene actualizado como el actual.
class MetadataHasMany < MetadataRelatedPosts
  # Todos los Post relacionados
  def has_many
    return default_value if value.blank?

    posts.where(uuid: value)
  end

  # La relación anterior
  def had_many
    return default_value if value_was.blank?

    posts.where(uuid: value_was)
  end

  def inverse?
    inverse.present?
  end

  # La relación inversa
  #
  # @return [Nil,Symbol]
  def inverse
    @inverse ||= layout.metadata.dig(name, 'inverse')&.to_sym
  end

  # Actualizar las relaciones inversas.  Hay que buscar la diferencia
  # entre had y has_many.
  def save
    super

    return true unless changed?
    return true unless inverse?

    (had_many - has_many).each do |remove|
      remove[inverse]&.value = remove[inverse].default_value
    end

    (has_many - had_many).each do |add|
      add[inverse]&.value = post.uuid.value
    end

    true
  end

  def related_posts?
    true
  end

  def related_methods
    @related_methods ||= %i[has_many had_many].freeze
  end
end
