# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class ContactControllerTest < ActionDispatch::IntegrationTest
      setup do
        @rol = create :rol
        @site = @rol.site
        @usuarie = @rol.usuarie

        @site.update contact: true, design: Design.find_by_gem('editorial-autogestiva-jekyll-theme')
        @site.config.write
        @site.reload
        @host = { host: "api.#{Site.domain}" }
      end

      teardown do
        @site&.destroy
      end

      test 'el sitio tiene que existir' do
        @site.destroy

        get v1_site_contact_cookie_url(@site.hostname, **@host)

        assert_not cookies[@site.name]

        post v1_site_contact_url(site_id: @site.hostname, form: :contacto, **@host),
             params: {
               name: SecureRandom.hex,
               pronouns: SecureRandom.hex,
               contact: SecureRandom.hex,
               from: "#{SecureRandom.hex}@sutty.nl",
               body: SecureRandom.hex,
               consent: true
             }

        assert_response :precondition_required
        assert_equal 'expired_or_invalid_cookie', response.body
      end

      test 'hay que enviar desde el sitio principal' do
        get v1_site_contact_cookie_url(@site.hostname, **@host)
        post v1_site_contact_url(site_id: @site.hostname, form: :contacto, **@host),
             params: {
               name: SecureRandom.hex,
               pronouns: SecureRandom.hex,
               contact: SecureRandom.hex,
               from: "#{SecureRandom.hex}@sutty.nl",
               body: SecureRandom.hex,
               consent: true
             }

        assert_response :precondition_required
        assert_equal 'site_is_not_origin', response.body
      end

      test 'hay que dar consentimiento' do
        get v1_site_contact_cookie_url(@site.hostname, **@host)
        post v1_site_contact_url(site_id: @site.hostname, form: :contacto, **@host),
             headers: {
               origin: @site.url
             },
             params: {
               name: SecureRandom.hex,
               pronouns: SecureRandom.hex,
               contact: SecureRandom.hex,
               from: "#{SecureRandom.hex}@sutty.nl",
               body: SecureRandom.hex
             }

        assert_response :precondition_required
        assert_equal 'no_consent', response.body
      end

      test 'enviar un mensaje genera correos' do
        ActionMailer::Base.deliveries.clear

        redirect = "#{@site.url}?thanks"

        10.times do
          create :rol, site: @site
        end

        get v1_site_contact_cookie_url(@site.hostname, **@host)
        post v1_site_contact_url(site_id: @site.hostname, form: :contacto, **@host),
             headers: {
               Origin: @site.url
             },
             params: {
               name: SecureRandom.hex,
               pronouns: SecureRandom.hex,
               contact: SecureRandom.hex,
               from: "#{SecureRandom.hex}@sutty.nl",
               body: SecureRandom.hex,
               consent: true,
               redirect: redirect
             }

        assert_equal redirect, response.headers['Location']
        assert_equal 2, ActionMailer::Base.deliveries.size
      end

      test 'se puede enviar mensajes a dominios propios' do
        ActionMailer::Base.deliveries.clear

        @site.update name: 'example.org.'

        redirect = "#{@site.url}?thanks"

        10.times do
          create :rol, site: @site
        end

        get v1_site_contact_cookie_url(@site.hostname, **@host)
        post v1_site_contact_url(site_id: @site.hostname, form: :contacto, **@host),
             headers: { origin: @site.url },
             params: {
               name: SecureRandom.hex,
               pronouns: SecureRandom.hex,
               contact: SecureRandom.hex,
               from: "#{SecureRandom.hex}@sutty.nl",
               body: SecureRandom.hex,
               consent: true,
               redirect: redirect
             }

        assert_equal redirect, response.headers['Location']
        assert_equal 2, ActionMailer::Base.deliveries.size
      end
    end
  end
end
