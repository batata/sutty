# frozen_string_literal: true

# Modificaciones para Blazer
module BlazerDecorator
  # No poder obtener información de la base de datos.
  module DisableDatabaseInfo
    extend ActiveSupport::Concern

    included do
      def docs; end

      def tables; end

      def schema; end
    end
  end

  # Deshabilitar edición de consultas y chequeos.
  module DisableEdits
    extend ActiveSupport::Concern

    included do
      def create; end

      def update; end

      def destroy; end

      def run; end

      def refresh; end

      def cancel; end
    end
  end

  # Blazer hace un gran esfuerzo para ejecutar consultas de forma
  # asincrónica pero termina enviándolas por JS.
  module RunSync
    extend ActiveSupport::Concern

    included do
      alias_method :original_show, :show

      include Blazer::BaseHelper

      def show
        original_show

        options = { user: blazer_user, query: @query, run_id: SecureRandom.uuid, async: false }
        @data_source = Blazer.data_sources[@query.data_source]
        @result = Blazer::RunStatement.new.perform(@data_source, @statement, options)
        chart_data
      end

      private

      # Solo mostrar las consultas de le usuarie
      def set_queries(_ = nil)
        @queries = (@current_usuarie || current_usuarie).blazer_queries
      end

      # blazer-2.4.2/app/views/blazer/queries/run.html.erb
      def chart_type
        case @result.chart_type
        when /\Aline(2)?\z/
          chart_options.merge! min: nil
        when /\Abar(2)?\z/
          chart_options.merge! library: { tooltips: { intersect: false, axis: 'x' } }
        when 'pie'
          chart_options
        when 'scatter'
          chart_options.merge! library: { tooltips: { intersect: false } }, xtitle: @result.columns[0],
                               ytitle: @result.columns[1]
        when nil
        else
          if @result.column_types.size == 2
            chart_options.merge! library: { tooltips: { intersect: false, axis: 'x' } }
          else
            chart_options.merge! library: { tooltips: { intersect: false } }
          end
        end

        @result.chart_type
      end

      def chart_data
        @chart_data ||=
          case chart_type
          when 'line'
            @result.columns[1..-1].each_with_index.map do |k, i|
              {
                name: blazer_series_name(k),
                data: @result.rows.map do |r|
                  [r[0], r[i + 1]]
                end,
                library: series_library[i]
              }
            end
          when 'line2'
            @result.rows.group_by do |r|
              v = r[1]
              (@result.boom[@result.columns[1]] || {})[v.to_s] || v
            end.each_with_index.map do |(name, v), i|
              {
                name: blazer_series_name(name),
                data: v.map do |v2|
                  [v2[0], v2[2]]
                end,
                library: series_library[i]
              }
            end
          when 'pie'
            @result.rows.map do |r|
              [(@result.boom[@result.columns[0]] || {})[r[0].to_s] || r[0], r[1]]
            end
          when 'bar'
            (@result.rows.first.size - 1).times.map do |i|
              name = @result.columns[i + 1]

              {
                name: blazer_series_name(name),
                data: @result.rows.first(20).map do |r|
                  [(@result.boom[@result.columns[0]] || {})[r[0].to_s] || r[0], r[i + 1]]
                end
              }
            end
          when 'bar2'
            first_20 = @result.rows.group_by { |r| r[0] }.values.first(20).flatten(1)
            labels = first_20.map { |r| r[0] }.uniq
            series = first_20.map { |r| r[1] }.uniq
            labels.each do |l|
              series.each do |s|
                first_20 << [l, s, 0] unless first_20.find { |r| r[0] == l && r[1] == s }
              end
            end

            first_20.group_by do |r|
              v = r[1]
              (@result.boom[@result.columns[1]] || {})[v.to_s] || v
            end.each_with_index.map do |(name, v), _i|
              {
                name: blazer_series_name(name),
                data: v.sort_by do |r2|
                  labels.index(r2[0])
                end.map do |v2|
                  v3 = v2[0]
                  [(@result.boom[@result.columns[0]] || {})[v3.to_s] || v3, v2[2]]
                end
              }
            end
          when 'scatter'
            @result.rows
          end
      end

      def target_index
        @target_index ||= @result.columns.index do |k|
          k.downcase == 'target'
        end
      end

      def series_library
        @series_library ||= {}.tap do |sl|
          if target_index
            color = '#109618'
            sl[target_index - 1] = {
              pointStyle: 'line',
              hitRadius: 5,
              borderColor: color,
              pointBackgroundColor: color,
              backgroundColor: color,
              pointHoverBackgroundColor: color
            }
          end
        end
      end

      def chart_options
        @chart_options ||= { id: SecureRandom.hex }
      end
    end
  end
end

classes = [Blazer::QueriesController, Blazer::ChecksController, Blazer::DashboardsController]
modules = [BlazerDecorator::DisableDatabaseInfo, BlazerDecorator::DisableEdits]
classes.each do |klass|
  modules.each do |modul|
    klass.include modul unless klass.included_modules.include? modul
  end
end

Blazer::QueriesController.include BlazerDecorator::RunSync
