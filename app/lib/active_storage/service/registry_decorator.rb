# frozen_string_literal: true

module ActiveStorage
  class Service
    # Modificaciones a ActiveStorage::Service::Registry
    module RegistryDecorator
      extend ActiveSupport::Concern

      included do
        # El mismo comportamiento que #fetch con el agregado de generar
        # un {JekyllService} para cada sitio.
        def fetch(name)
          services.fetch(name.to_sym) do |key|
            if configurations.include?(key)
              services[key] = configurator.build(key)
            elsif (site = Site.find_by_name(key))
              services[key] = ActiveStorage::Service::JekyllService.build_for_site(site: site)
            elsif block_given?
              yield key
            else
              raise KeyError, "Missing configuration for the #{key} Active Storage service. " \
                              "Configurations available for the #{configurations.keys.to_sentence} services."
            end
          end
        end
      end
    end
  end
end

ActiveStorage::Service::Registry.include ActiveStorage::Service::RegistryDecorator
