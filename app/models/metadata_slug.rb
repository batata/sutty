# frozen_string_literal: true

require 'jekyll/utils'

# El slug es el nombre del archivo sin la fecha ni la extensión y se
# deriva del título.
#
# Si el slug del document es distinto al que le asignaría el título
# slugificado, quiere decir que lo indicamos manualmente y no hay que
# cambiarlo.
#
# Pero si cambiamos el slug manualmente, tenemos que darle prioridad a
# ese cambio.
#
# El slug por defecto es el asignado por el documento.
#
# Si no hay slug, slugificamos el título.
#
# Si cambiamos el título y el slug coincide con el slug del título
# anterior, también lo cambiamos para mantener las URLs consistentes.
# Luego podemos usar el plugin que guarda los slugs anteriores para
# generar links.
#
# TODO: Transliterar tildes?
class MetadataSlug < MetadataTemplate
  # Trae el slug desde el título si existe o una string al azar
  def default_value
    title ? Jekyll::Utils.slugify(title, mode: site.slugify_mode) : SecureRandom.uuid
  end

  def value
    self[:value] ||= document.data.fetch('slug', default_value)
  end

  private

  # Devuelve el título a menos que sea privado y no esté vacío
  def title
    return if post.title&.private?
    return if post.title&.value&.blank?

    post.title&.value&.to_s&.unicode_normalize
  end
end
