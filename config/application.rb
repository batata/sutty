# frozen_string_literal: true

require_relative 'boot'

require 'aasm'
require 'redis-client'
require 'hiredis-client'
require 'brs'
require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
# require 'action_mailbox/engine'
require 'action_text/engine'
require 'action_view/railtie'
# require "action_cable/engine"
require 'sprockets/railtie'
require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

if %w[development test].include? ENV['RAILS_ENV']
  # https://github.com/bkeepers/dotenv/pull/453
  Dotenv::Railtie.class_eval do
    def overload
      Dotenv.overload(*dotenv_files.reverse)
    end
  end

  Dotenv::Railtie.overload
end

module Sutty
  ALLOWED_ATTRIBUTES = %w[style href src alt controls data-align data-multimedia data-multimedia-inner id name rel
                          target referrerpolicy class colspan rowspan role data-turbo start type reversed].freeze
  ALLOWED_TAGS = %w[strong em del u mark p h1 h2 h3 h4 h5 h6 ul ol li img iframe audio video div figure blockquote
                    figcaption a sub sup small table thead tbody tfoot tr th td br code].freeze

  # Sutty!
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails
    # version.
    config.load_defaults 6.1

    # Settings in config/environments/* take precedence over those
    # specified here.  Application configuration can go into files in
    # config/initializers -- all .rb files in that directory are
    # automatically loaded after loading the framework and any gems in
    # your application.
    config.action_dispatch
          .rescue_responses['Pundit::NotAuthorizedError'] = :forbidden

    config.active_storage.variant_processor = :vips
    config.active_storage.web_image_content_types << 'image/webp'

    # Que
    config.action_mailer.deliver_later_queue_name = :default
    config.active_storage.queues.analysis         = :default
    config.active_storage.queues.purge            = :default
    config.active_job.queue_adapter               = :que

    config.active_record.schema_format = :sql

    config.to_prepare do
      # Load application's model / class decorators
      Dir.glob(File.join(File.dirname(__FILE__), '..', 'app', '**', '*_decorator.rb')).sort.each do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end

    config.after_initialize do
      ActiveStorage::DirectUploadsController.include ActiveStorage::AuthenticatedDirectUploadsController

      I18n.available_locales.each do |locale|
        translations = I18n.t(:email_address, locale: locale)

        next unless translations.is_a? Hash

        EmailAddress::Config.error_messages translations.transform_keys(&:to_s), locale.to_s
      end
    end

    def nodes
      @nodes ||= ENV.fetch('SUTTY_NODES', '').split(',')
    end
  end
end
