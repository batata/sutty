# frozen_string_literal: true

# = Image =
#
# Representa artículos
class ActivityPub
  class Object
    class Image < ActivityPub::Object; end
  end
end
