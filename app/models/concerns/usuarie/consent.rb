# frozen_string_literal: true

class Usuarie
  # Gestiona los campos de consentimiento
  module Consent
    extend ActiveSupport::Concern

    included do
      CONSENT_FIELDS = %i[privacy_policy_accepted terms_of_service_accepted code_of_conduct_accepted available_for_feedback_accepted]

      CONSENT_FIELDS.each do |field|
        attribute field, :boolean
      end

      before_save :update_consent_fields!

      private

      def update_consent_fields!
        CONSENT_FIELDS.each do |field|
          send(:"#{field}_at=", Time.now) if send(field).present?
        end
      end
    end
  end
end
