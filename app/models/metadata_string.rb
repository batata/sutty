# frozen_string_literal: true

# Un campo de texto
class MetadataString < MetadataTemplate
  # Una string vacía
  def default_value
    super || ''
  end

  def indexable?
    true && !private?
  end

  private

  # No se permite HTML en las strings
  def sanitize(string)
    return '' if string.blank?

    sanitizer.sanitize(string.strip.unicode_normalize,
                       tags: [],
                       attributes: []).strip.html_safe
  end
end
