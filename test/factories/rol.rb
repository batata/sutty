# frozen_string_literal: true

FactoryBot.define do
  factory :rol do
    usuarie
    site
    rol { 'usuarie' }
    temporal { false }

    factory :rol_invitade do
      rol { 'invitade' }
    end
  end
end
