# frozen_string_literal: true

# Gestiona acciones de moderación
class ActivityPubsController < ApplicationController
  include ModerationConcern

  ActivityPub.events.each do |event|
    define_method(event) do
      authorize activity_pub

      if event == :report
        remote_flag_params(activity_pub).tap do |p|
          activity_pub.remote_flag_id = p[:remote_flag_attributes][:id]
          activity_pub.update(p)
        end
      end

      message =
        if activity_pub.public_send(:"may_#{event}?") && activity_pub.public_send(:"#{event}!")
          :success
        else
          :error
        end

      flash[message] = I18n.t("activity_pubs.#{event}.#{message}")

      redirect_to_moderation_queue!
    end
  end

  def action_on_several
    redirect_to_moderation_queue!

    activity_pubs = site.activity_pubs.where(id: params[:activity_pub])

    return if activity_pubs.count.zero?

    authorize activity_pubs

    action = params[:activity_pub_action].to_sym
    method = :"#{action}_all!"
    may    = :"may_#{action}?"

    return unless ActivityPub.events.include? action

    # Crear una sola remote flag por autore
    ActivityPub.transaction do
      if action == :report
        message = remote_flag_params(activity_pubs.first).dig(:remote_flag_attributes, :message)

        activity_pubs.distinct.pluck(:actor_id).each do |actor_id|
          remote_flag = ActivityPub::RemoteFlag.find_or_initialize_by(actor_id: actor_id, site_id: site.id)
          remote_flag.message = message
          # Lo estamos actualizando, con lo que lo vamos a volver a enviar
          remote_flag.requeue if remote_flag.persisted?
          remote_flag.save
          # XXX: Idealmente todas las ActivityPub que enviamos pueden
          # cambiar de estado, pero chequeamos de todas formas.
          remote_flag.activity_pubs << (activity_pubs.where(actor_id: actor_id).to_a.select { |a| a.public_send(may) })
        end
      end

      message = activity_pubs.public_send(method) ? :success : :error

      flash[message] = I18n.t("activity_pubs.action_on_several.#{message}")
    end
  end

  private

  def activity_pub
    @activity_pub ||= site.activity_pubs.find(params[:activity_pub_id])
  end
end
