# Glosario

El glosario permite describir categorías y etiquetas y normalizar
sinónimos.

## Creación

Al crear una etiqueta o categoría en el formulario de categorías, se
crea un artículo dentro de la categoría "Glosario" y layout
"glosario".

XXX: En el futuro, cuando Sutty soporte colecciones, se moverán a la
colección `_glosario`.

Estos artículos pueden editarse desde el editor normal de Sutty,
agregando definiciones y comportándose como un artículo más.

## Preguntas

Si el glosario es una colección, cómo se pueden traducir?  Van a ser
`_glosario_idioma` o `_idioma_glosario`?  Lo mismo para la i18n de otras
colecciones.

## Select2

En el selector de categorías y tags, traer el contenido del glosario y
sinónimos como sugerencias.

TODO hacer todo esto como una clase aparte e incluirla en el modelo
Post, lo mismo para las plantillas y otras features específicas.
Incluso se las puede incluir solo cuando están activadas para el sitio.
Organiza mejor el código.
