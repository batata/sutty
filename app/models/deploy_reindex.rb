# frozen_string_literal: true

# Reindexa los artículos al terminar la compilación
class DeployReindex < Deploy
  def deploy(**)
    time_start

    site.reset

    Site.transaction do
      site.indexed_posts.destroy_all
      site.index_posts!
    end

    time_stop

    build_stats.create action: 'reindex',
                       log: 'Reindex',
                       seconds: time_spent_in_seconds,
                       bytes: size,
                       status: true
    site.touch
  end

  def size
    0
  end

  def limit
    1
  end

  def hostname; end

  def url; end

  def destination; end
end
