# frozen_string_literal: true

# = Event =
#
# Representa artículos
class ActivityPub
  class Object
    class Event < ActivityPub::Object; end
  end
end
