process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const CircularDependencyPlugin = require('circular-dependency-plugin');
const path = require("path");

const environment = require('./environment')

environment.plugins.append(
  "ForkTsCheckerWebpackPlugin",
  new ForkTsCheckerWebpackPlugin({
    typescript: {
		configFile: path.resolve(__dirname, "../../tsconfig.json"),
	},
    // non-async so type checking will block compilation
    async: false,
  }),
);
environment.plugins.append(
  "CircularDependencyPlugin",
  new CircularDependencyPlugin({
    exclude: /node_modules/,
    failOnError: false,
    // allow import cycles that include an asyncronous import,
    // e.g. via import(/* webpackMode: "weak" */ './file.js')
    allowAsyncCycles: false,
    cwd: process.cwd(),
  }),
)

module.exports = environment.toWebpackConfig()
