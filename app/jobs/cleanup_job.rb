# frozen_string_literal: true

# Realiza tareas de limpieza en segundo plano
class CleanupJob < ApplicationJob
  def perform(before = nil)
    CleanupService.new(before: before).cleanup_everything!
  end
end
