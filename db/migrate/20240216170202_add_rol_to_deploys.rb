# frozen_string_literal: true

# Establece una relación entre roles y deploys
class AddRolToDeploys < ActiveRecord::Migration[6.1]
  def up
    add_column :deploys, :rol_id, :integer, index: true

    Deploy.find_each do |deploy|
      rol_id = deploy.site.roles.find_by(rol: 'usuarie', temporal: false)&.id

      deploy.update_column(:rol_id, rol_id) if rol_id
    end
  end

  def down
    remove_column :deploys, :rol_id
  end
end
