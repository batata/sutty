# Subida de archivos

La subida de archivos se genera a partir de agregar el valor a una
plantilla:

```yaml
---
cover:
  type: image
attachment:
  type: document
video:
  type: video
audio:
  type: audio
---
```

Donde `image` solo admite imágenes, `document` cualquier tipo de
documento y `video` y `audio` medios.

Al subir los archivos, se guardan en el directorio `public/` en la raíz
del sitio.

En el frontmatter solo sale la URL del archivo asociado.

## ActiveStorage

ActiveStorage es el método por defecto para subida de archivos en Ruby
on Rails.  Sin embargo, no nos permite decidir dónde queremos guardar
los archivos, sino que los guarda directamente relacionados a un modelo
de la base de datos, en una ruta generada automáticamente.

Podríamos implementar un proveedor de ActiveStorage, pero la
documentación para hacerlo es escasa.

Como tenemos que asociarlo a un modelo, lo correcto sería usar el modelo
Site, de forma que tengamos acceso a todas las imágenes.  Esto abriría
la posibilidad de tener una galería de imágenes y poder seleccionar
imágenes entre las ya subidas o subir nuevas.

Pero como los archivos físicos se guardan directamente en una ruta
indicada por ActiveStorage, empezamos a utilizar más espacio porque para
que los sitios sean autocontenidos, deberíamos copiarlos al directorio
del sitio.  También podemos utilizar enlaces duros (_hardlinks_) para no
ocupar espacio extra.

Ya que las imágenes van a estar dentro del directorio de Sutty en lugar
de cada archivo, es más fácil poder visualizarlas, sin tener que hacer
hacks enviando el archivo a través de Rails.  Con AS podemos vincularlas
directamente desde el sistema de archivos.

## CarrierWave

CarrierWave es la forma en que estamos subiendo los archivos hasta el
momento.  Puede transformar las imágenes a distintas resoluciones
automáticamente (aunque quisiéramos hacer esto durante la generación del
sitio).  También hemos tenido problemas con los nombres de los archivos
y encontrado errores no documentados con respecto a cuándo cambia el
nombre del archivo o no, con lo que usar CW se nos hizo un poco endeble.

## Subida

Entonces:

* Activamos ActiveStorage local (ofrece nubes, pero no queremos ninguna
  nube)

* Asociamos archivos al modelo Site

* Al subir un archivo desde el editor de Post, se asocia al Site, no al
  Post.

* Para asociar el archivo al Post, generamos un hardlink con la misma
  ruta, dentro del directorio del sitio.  Esto nos permite vincular
  ambos archivos sin agregar metadatos adicionales.

* Para generar variantes del archivo, lo hacemos con un plugin Jekyll
  que las genera dentro del sitio si no existen y que agrega los
  atributos srcset para imágenes responsive.

* Si encontramos forma, aplicamos `oxipng` y otros optimizadores.  Sino,
  lo haremos desde el plugin.

## Dependencias

Usamos VIPS para procesar imágenes con bajo consumo de recursos

## TODO

* Crear una vista de galería
* Poder elegir imágenes desde la galería
* Optimizar las imágenes subidas
  * Crear un paquete de oxipng para Alpine
