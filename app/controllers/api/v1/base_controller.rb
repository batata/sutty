# frozen_string_literal: true

module Api
  module V1
    # API
    class BaseController < ActionController::Base
      include ExceptionHandler

      protect_from_forgery with: :null_session
      respond_to :json

      private

      # Realiza la inversa de Site#hostname
      #
      # TODO: El sitio sutty.nl no aplica a ninguno de estos y le
      # tuvimos que poner 'sutty.nl..sutty.nl' para pasar el test.
      def site_id
        @site_id ||= if params[:site_id].end_with? Site.domain
                       params[:site_id].sub(/\.#{Site.domain}\z/, '')
                     else
                       params[:site_id] + '.'
                     end
      end

      def origin
        request.headers['Origin']
      end

      # Los navegadores antiguos no envían Origin
      def origin?
        !origin.blank?
      end
    end
  end
end
