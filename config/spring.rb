# frozen_string_literal: true

# Ignorar los sitios Jekyll
# https://github.com/thoughtbot/ember-cli-rails/issues/479#issuecomment-241302961
Spring::Watcher::Listen.class_eval do
  def base_directories
    %w[app config db lib test vendor]
      .uniq.map { |path| Pathname.new(File.join(root, path)) }
  end
end

Spring.watch(
  '.ruby-version',
  '.rbenv-vars',
  'tmp/restart.txt',
  'tmp/caching-dev.txt'
)
