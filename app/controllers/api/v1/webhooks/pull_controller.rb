# frozen_string_literal: true

module Api
  module V1
    module Webhooks
      # Recibe webhooks y lanza un PullJob
      class PullController < BaseController
        include Api::V1::Webhooks::Concerns::WebhookConcern

        # Trae los cambios a partir de un post de Webhooks:
        # (Gitlab, Github, Gitea, etc)
        #
        # @return [nil]
        def pull
          message = I18n.with_locale(site.default_locale) do
            I18n.t('webhooks.pull.message')
          end

          GitPullJob.perform_later(site, usuarie, message)
          head :ok
        end
      end
    end
  end
end
