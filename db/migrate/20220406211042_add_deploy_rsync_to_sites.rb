# frozen_string_literal: true

# Agrega un DeployRsync hacia los servidores alternativos para cada
# sitio
class AddDeployRsyncToSites < ActiveRecord::Migration[6.1]
  def up
    Site.find_each do |site|
      SiteService.new(site: site).send :sync_nodes
      site.save
    end
  end

  def down
    DeployRsync.destroy_all
  end
end
