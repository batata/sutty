# frozen_string_literal: true

# Envía un reporte directamente a la instancia remota
#
# @todo El panel debería ser su propia instancia y firmar sus propios
# mensajes.
# @todo Como la Social Inbox no soporta enviar actividades
# a destinataries que no sean seguidores, enviamos el reporte
# directamente a la instancia.
# @see {https://github.com/hyphacoop/social.distributed.press/issues/14}
class ActivityPub
  class RemoteFlagJob < ApplicationJob
    self.priority = 30

    def perform(remote_flag:)
      return unless remote_flag.may_queue?

      inbox = remote_flag.actor&.content&.[]('inbox')

      raise 'Inbox is missing for actor' if inbox.blank?

      remote_flag.queue!

      uri = URI.parse(inbox)
      client = remote_flag.main_site.social_inbox.client_for(uri.origin)
      response = client.post(endpoint: uri.path, body: remote_flag.content)

      raise 'No se pudo enviar el reporte' unless response.success?

      remote_flag.report!
    rescue Exception => e
      ExceptionNotifier.notify_exception(e, data: { remote_flag: remote_flag.id, response: response.parsed_response })
      remote_flag.resend!
      raise
    end
  end
end
