# frozen_string_literal: true

# Almacenar el último commit indexado
class AddLastIndexedCommitToSites < ActiveRecord::Migration[6.1]
  def up
    add_column :sites, :last_indexed_commit, :string, null: true

    Site.find_each do |site|
      site.update_columns(last_indexed_commit: site.repository.head_commit.oid)
    rescue Rugged::Error, Rugged::OSError => e
      puts "Falló #{site.name}, ignorando: #{e.message}"
    end
  end

  def down
    remove_column :sites, :last_indexed_commit
  end
end
