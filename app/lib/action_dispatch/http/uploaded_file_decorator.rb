# frozen_string_literal: true

module ActionDispatch
  module Http
    # Normaliza los nombres de archivo para que se propaguen
    # correctamente a través de todo el stack.
    module UploadedFileDecorator
      extend ActiveSupport::Concern

      included do
        # Devolver el nombre de archivo con caracteres unicode
        # normalizados
        def original_filename
          @original_filename.unicode_normalize.sub(/\A_+/, '')
        end
      end
    end
  end
end

ActionDispatch::Http::UploadedFile.include ActionDispatch::Http::UploadedFileDecorator
