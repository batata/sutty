# frozen_string_literal: true

# Crea la tabla de licencias
class CreateLicencias < ActiveRecord::Migration[5.2]
  def change
    create_table :licencias do |t|
      t.timestamps
      t.string :name, unique: true
      t.text   :description
      t.text   :deed
      t.string :url
    end

    add_belongs_to :sites, :licencia, index: true
  end
end
