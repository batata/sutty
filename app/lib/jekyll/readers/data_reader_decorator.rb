# frozen_string_literal: true

module Jekyll
  module Readers
    # Permite leer datos utilizando rutas absolutas.
    #
    # {Jekyll::DataReader} usa {Dir.chdir} con rutas relativas, lo que
    # en nuestro uso provoca confusiones en el lector de datos.
    #
    # Con este módulo, podemos leer todos los archivos usando rutas
    # absolutas, lo que nos permite reemplazar jekyll-data, que agregaba
    # código duplicado.
    module DataReaderDecorator
      extend ActiveSupport::Concern

      included do
        DATA_EXTENSIONS = %w[.yaml .yml .json .csv .tsv].freeze

        def read_data_to(dir, data)
          return unless File.directory?(dir) && !@entry_filter.symlink?(dir)

          Dir.glob(File.join(dir, '*')).each do |path|
            next if @entry_filter.symlink?(path)

            entry = Pathname.new(path).relative_path_from(dir).to_s

            if File.directory?(path)
              read_data_to(path, data[sanitize_filename(entry)] = {})
            elsif DATA_EXTENSIONS.include?(File.extname(entry))
              key = sanitize_filename(File.basename(entry, ".*"))
              data[key] = read_data_file(path)
            end
          end
        end
      end
    end
  end
end
