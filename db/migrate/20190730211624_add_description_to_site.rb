# frozen_string_literal: true

# Agrega la descripción de un sitio
class AddDescriptionToSite < ActiveRecord::Migration[5.2]
  def change
    add_column :sites, :description, :text
  end
end
