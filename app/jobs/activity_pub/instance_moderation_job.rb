# frozen_string_literal: true

class ActivityPub
  # Bloquea varias instancias de una sola vez
  class InstanceModerationJob < ApplicationJob
    # @param :site [Site]
    # @param :hostnames [Array<String>]
    # @param :perform_remotely [Bool]
    def perform(site:, hostnames:, perform_remotely: true)
      # Crear las instancias que no existan todavía
      hostnames.each do |hostname|
        ActivityPub::Instance.lock.find_or_create_by(hostname: hostname)
      end

      instances = ActivityPub::Instance.where(hostname: hostnames)

      Site.transaction do
        # Crea todas las moderaciones de instancia con un estado por
        # defecto si no existen
        instances.find_each do |instance|
          # Esto bloquea cada una individualmente en la Social Inbox,
          # idealmente son pocas instancias las que aparecen.
          site.instance_moderations.lock.find_or_create_by(instance: instance)
        end

        scope = site.instance_moderations.where(instance_id: instances.ids)

        if perform_remotely
          scope.block_all!
        else
          scope.block_all_without_callbacks!
        end

        ActivityPub::SyncListsJob.perform_later(site: site)
      end
    end
  end
end
