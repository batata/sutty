# Colaboraciones anónimas

> Estamos escribiendo hipótesis para aclararnos las ideas.


[Ver discusión](https://0xacab.org/sutty/sutty/issues/75)

## Configuración

```yaml
# Actual
invitades: true

# Nueva
invitades:
  allowed:
  - users
  - guests
  - anonymous
```

Pero en realidad no queremos instanciar todo el sitio y leer la
configuración para poder comprobar esto, así que lo movemos a la base de
datos.  De todas formas es información que no tiene sentido almacenar en
`_config.yml` porque no tiene uso fuera de Sutty.


## Procedimiento

* Al cargar el formulario, se incorpora una petición a la API de Sutty
  que devuelve un recurso vacío y una cookie cifrada solo disponible
  para HTTP (no para JS).  Además agrega una cookie de sesión con un
  token anti CSRF.  Les decimos cookie-token y cookie-sesión
  respectivamente.

* Al enviar el formulario, la petición se envía con estas cookies.  Si
  los tokens coinciden, el envío se permite.  Esto no es una protección
  CSRF completa, sino una forma de validar que se solicitó una cookie
  antes.

* La sesión es válida si el token de sesión y el de la cookie coinciden.

* La emisión de sesiones + cookies está limitada en el servidor.

* Al cargar los datos correctamente, respondemos con una redirección
  a la página de agradecimiento.


### CSRF

* Las protecciones contra CSRF permitirían al sitio que envía obtener un
  token de autenticación que se valida contra la cookie de sesión
  enviada por el servidor al pedir el recurso.

* El sitio tiene que enviar este token junto con la petición.

* No tiene mucho sentido usar protección contra CSRF porque ya estamos
  haciendo peticiones cruzadas.  La protección contra CSRF previene
  acciones que en realidad queremos realizar (!)

* Trabajar con protección CSRF requiere que el sitio use JS que no
  estábamos dispuestes a utilizar, porque hay que tomar el token
  e incorporarlo al formulario en forma de campo oculto.  Queremos que
  les visitantes con JS deshabilitado puedan interactuar con nuestros
  formularios también!

* La validación que estamos haciendo entre cookie cifrada y fecha de
  vencimiento de la cookie es una forma de protección contra CSRF
  liviana y sin interacción, aprovechando los mecanismos del navegador.

* Estamos mirando el valor de Origin para prevenir 

### XSS

* Es importante limpiar todos las entradas de valores, para proteger
  a les usuaries del sitio de ataques mayores, por ejemplo que se
  introduzca JS en un artículo que luego se abre desde el panel.

* Hay que chequear las protecciones CSRF en los formularios internos del
  panel!

* Hay que escapar todas las entradas al mostrarlas!

* Si la redirección se obtiene desde el mismo formulario, estaría
  abierto a XSS?

### DOS

* La idea es permitir el envío de colaboraciones a una tasa normal (1
  cada X minutos) y dificultar las tasas de envío agresivas (miles por
  segundo).

* El DOS no solo implica bajar el servidor, sino también llenar el sitio
  de artículos basura y dificultar el uso.  O sea, el DOS se aplica
  a les usuaries, que se estresan.

* Las cookies se pueden reutilizar, siempre y cuando el token sea
  válido.  Si guardáramos otra información como cantidad de
  utilizaciones de una cookie, tendríamos que guardar el estado en la
  base de datos y no queremos usar recursos en esto.

* Los atacantes pueden descartar la cookie (volverla a emitir)
  o utilizar siempre la misma.

* Las cookies se emiten con límite, una cada 5 minutos.

* La cookie tiene que durar lo que se puede tardar en cargar el
  formulario y completarlo, con un excedente por las dudas.  Los
  formularios tienen que soportar el guardado offline / autocompletado
  para evitar que les usuaries se frustren.  También es posible hacer la
  solicitud de cookie usando JS inmediatamente antes del envío para
  reducir la duración de la cookie aun más.

* Si la validez de la cookie-token es mayor que la tasa de emisión
  (digamos, dura 30 minutos), un atacante puede enviar todos los
  artículos que quiera durante ese tiempo (miles), con lo que tendríamos
  que llevar un estado de las sesiones de todas formas.

* El envío de información también puede tener tasa de petición.  Si
  aplicamos `rate limit` en nginx a X minutos entre cada una, tiene que
  haber una diferencia de tiempo entre la emisión de la cookie y el
  envío de la información.  Si la cookie se reintenta usar muchas veces,
  también aplica la limitación.

* Si la tasa de envío es cada 5 minutos, un atacante podría enviar 288
  artículos por día desde una sola IP.

## Casos de uso

* Usuarie ingresa al sitio, completa el formulario y lo envía.

  Este es el caso que queremos.

* Dogpiling: Usuarios maliciosos pero desorganizados ingresan al sitio,
  completan el formulario muchas veces y lo envían.

  Para este caso estaríamos protegides por el rate limit.  No tenemos
  protección contra la paciencia y perseverancia del odio.

  Quizás les podamos empezar a enviar zip bombs.

* DOS: Atacante individual o colectivo genera script que envía muchas
  veces el formulario automáticamente.  Puede enviar desde muchas
  computadoras y es capaz de entender nuestra protecciones para
  encontrarles puntos débiles.

  Nos protege el rate limit hasta cierto punto.

* DDOS: Los atacantes aprovechan la capacidad de muchas personas que
  voluntaria o inconcientemente ingresan a una URL que es capaz de enviar
  información basura.

  Nos protege el rate limit, CORS y XSS.

## Atención

* Sanitizar todas las entradas

* No dejar que se modifique slug, date, orden y otros metadatos internos

  Quizás valga la pena redefinir cuáles son los parámetros anónimos
