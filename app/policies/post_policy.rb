# frozen_string_literal: true

# Política de acceso a artículos
class PostPolicy
  attr_reader :post, :usuarie

  def initialize(usuarie, post)
    @usuarie = usuarie
    @post = post
  end

  def index?
    true
  end

  # Les invitades solo pueden ver sus propios posts
  def show?
    post.site.usuarie?(usuarie) || post.usuaries.include?(usuarie)
  end

  def preview?
    show?
  end

  def new?
    create?
  end

  def create?
    true
  end

  def edit?
    update?
  end

  # Les invitades solo pueden modificar sus propios artículos
  def update?
    post.site.usuarie?(usuarie) || post.usuaries.include?(usuarie)
  end

  # Solo las usuarias pueden eliminar artículos.  Les invitades pueden
  # borrar sus propios artículos
  def destroy?
    update?
  end

  # Las usuarias pueden ver todos los posts
  #
  # Les invitades solo pueden ver sus propios posts
  class Scope
    attr_reader :usuarie, :scope

    def initialize(usuarie, scope)
      @usuarie = usuarie
      @scope = scope
    end

    def resolve
      return scope if scope&.first&.site&.usuarie? usuarie

      scope.by_usuarie(usuarie.id)
    end
  end
end
