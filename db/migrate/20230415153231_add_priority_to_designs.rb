class AddPriorityToDesigns < ActiveRecord::Migration[6.1]
  def change
    add_column :designs, :priority, :integer
  end
end
