class AddCrawlerToAccessLogs < ActiveRecord::Migration[6.0]
  def change
    return unless Rails.env.production?

    add_column :access_logs, :crawler, :boolean, default: false
  end
end
