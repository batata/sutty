# frozen_string_literal: true

require 'test_helper'

class SiteTest < ActiveSupport::TestCase
  def site
    @site ||= create :site
  end

  # Asegurarse que el sitio se destruye al terminar de usarlo
  teardown do
    site&.destroy
  end

  test 'se puede crear un sitio' do
    assert site.valid?
    # TODO: Mover a la validación del sitio o hacer algo similar
    assert File.directory?(site.path)
    assert File.directory?(File.join(site.path, '.git'))
    assert site.destroy
  end

  test 'el nombre tiene que ser único' do
    site2 = build :site, name: site.name

    assert_not site2.valid?
  end

  test 'el nombre del sitio puede contener subdominios' do
    @site = build :site, name: 'hola.chau'
    site.validate

    assert_not site.errors.messages[:name].present?
  end

  test 'el nombre del sitio puede terminar con punto' do
    @site = build :site, name: 'hola.chau.'
    site.validate

    assert_not site.errors.messages[:name].present?
  end

  test 'el nombre del sitio no puede contener wildcard' do
    @site = build :site, name: '*.chau'
    site.validate

    assert site.errors.messages[:name].present?
  end

  test 'el nombre del sitio solo tiene letras, numeros y guiones' do
    @site = build :site, name: 'A_Z!'
    site.validate

    assert site.errors.messages[:name].present?
  end

  test 'al destruir un sitio se eliminan los archivos' do
    @site = create :site
    assert site.destroy
    assert !File.directory?(site.path)
  end

  test 'se puede leer un sitio' do
    assert site.valid?
    assert !site.posts.empty?
  end

  test 'se pueden renombrar' do
    path = site.path

    site.update_attribute :name, SecureRandom.hex

    assert_not_equal path, site.path
    assert File.directory?(site.path)
    assert_not File.directory?(path)
  end

  test 'no se puede guardar html en title y description' do
    _site = build :site
    _site.description = "<a href='hola'>hola</a><script>alert('pwned')</script>"
    _site.title = "<a href='hola'>hola</a><script>alert('pwned')</script>"

    assert_equal 'hola', _site.description
    assert_equal 'hola', _site.title
  end

  test 'el sitio tiene artículos en distintos idiomas' do
    I18n.available_locales.each do |locale|
      assert site.posts(lang: locale).size.positive?
    end
  end

  test 'tienen un hostname que puede cambiar' do
    assert_equal "#{site.name}.#{Site.domain}", site.hostname

    site.name = name = SecureRandom.hex

    assert_equal "#{name}.#{Site.domain}", site.hostname
  end

  test 'se pueden traer los datos de una plantilla' do
    @site = create :site, design: Design.find_by(gem: 'editorial-autogestiva-jekyll-theme')

    assert_equal %i[post], site.layouts.to_h.keys

    site.config.write
    site.reload

    assert_equal %w[book cart confirmation editorial menu payment post shipment], site.data['layouts'].keys
    assert_equal %i[book cart confirmation editorial menu payment post shipment], site.layouts.to_h.keys
  end

  test 'se pueden encolar una sola vez' do
    assert site.enqueue!
    assert site.enqueued?
    assert_not site.enqueue!
  end
end
