# frozen_string_literal: true

class MetadataDate < MetadataTemplate
  def default_value
    Date.today
  end

  # Devuelve una fecha, si no hay ninguna es la fecha de hoy.
  #
  # @return [Date]
  def value
    return self[:value] if self[:value].is_a? Date
    return self[:value] if self[:value].is_a? Time
    return document_value || default_value unless self[:value]

    begin
      self[:value] = Date.parse self[:value]
    rescue ArgumentError, TypeError
      default_value
    end
  end
end
