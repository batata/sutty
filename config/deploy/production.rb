# frozen_string_literal: true

set :deploy_user, 'app'
set :deploy_to, '/srv/http/sutty.kefir.red'
set :branch, 'kefir'
set :rack_env, 'production'

set :tmp_dir, "#{fetch :deploy_to}/tmp"
set :bundle_path, '/srv/http/gems.kefir.red'

set :rbenv_type, :user
set :rbenv_ruby, '2.3.6'

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w[rake gem bundle ruby rails]
set :rbenv_roles, :all # default value

# Evitar compilar nokogiri
set :bundle_env_variables, nokogiri_use_system_libraries: 1

server 'miso',
       user: fetch(:deploy_user),
       roles: %w[app web db]
