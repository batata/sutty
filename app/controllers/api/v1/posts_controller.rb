# frozen_string_literal: true

module Api
  module V1
    # Recibe artículos desde colaboraciones anónimas
    class PostsController < ProtectedController
      # Crea un artículo solo si el sitio admite invitades
      def create
        # No procesar nada más si ya se aplicaron todos los filtros
        return if performed?

        usuarie = GitAuthor.new name: 'Anon', email: "anon@#{site.hostname}"
        service = PostService.new(params: params,
                                  site: site,
                                  usuarie: usuarie)

        site.touch if service.create_anonymous.persisted?

        # Redirigir a la URL de agradecimiento
        redirect_to params[:redirect_to] || origin.to_s
      end

      private

      def gave_consent?
        return if params[:consent].present?

        @reason = 'no_consent'
        render plain: Rails.env.production? ? nil : @reason, status: :precondition_required
      end

      def destination_exists?
        return if post? && site.layout?(params[:layout])

        @reason = 'layout_doesnt_exist'
        render plain: Rails.env.production? ? nil : @reason, status: :precondition_required
      end

      def from_is_address?
        true
      end
    end
  end
end
