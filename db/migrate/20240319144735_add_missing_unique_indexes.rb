# frozen_string_literal: true

# Parece que la sintaxis que veníamos usando para los índices únicos ya
# no es válida y por eso teníamos objetos duplicados.
class AddMissingUniqueIndexes < ActiveRecord::Migration[6.1]
  def up
    ActivityPub::Object.group(:uri).count.select { |_, v| v > 1 }.each_key do |uri|
      objects = ActivityPub::Object.where(uri: uri)
      deleted_ids = objects[1..].map(&:delete).map(&:id)

      ActivityPub.where(object_id: deleted_ids).update_all(object_id: objects.first.id, updated_at: Time.now)
    end

    ActivityPub::Actor.group(:uri).count.select { |_, v| v > 1 }.each_key do |uri|
      objects = ActivityPub::Actor.where(uri: uri)
      deleted_ids = objects[1..].map(&:delete).map(&:id)

      ActivityPub.where(actor_id: deleted_ids).update_all(actor_id: objects.first.id, updated_at: Time.now)
      ActorModeration.where(actor_id: deleted_ids).update_all(actor_id: objects.first.id, updated_at: Time.now)
      ActivityPub::Activity.where(actor_id: deleted_ids).update_all(actor_id: objects.first.id, updated_at: Time.now)
      ActivityPub::RemoteFlag.where(actor_id: deleted_ids).update_all(actor_id: objects.first.id, updated_at: Time.now)
    end

    ActivityPub::Instance.group(:hostname).count.select { |_, v| v > 1 }.each_key do |hostname|
      objects = ActivityPub::Instance.where(hostname: hostname)
      deleted_ids = objects[1..].map(&:delete).map(&:id)

      ActivityPub.where(instance_id: deleted_ids).update_all(instance_id: objects.first.id, updated_at: Time.now)
      InstanceModeration.where(instance_id: deleted_ids).update_all(instance_id: objects.first.id, updated_at: Time.now)
      ActivityPub::Actor.where(instance_id: deleted_ids).update_all(instance_id: objects.first.id, updated_at: Time.now)
    end

    remove_index :activity_pub_instances, :hostname
    remove_index :activity_pub_actors, :uri
    add_index :activity_pub_instances, :hostname, unique: true
    add_index :activity_pub_objects, :uri, unique: true
    add_index :activity_pub_actors, :uri, unique: true
  end

  def down
    remove_index :activity_pub_instances, :hostname, unique: true
    remove_index :activity_pub_objects, :uri, unique: true
    remove_index :activity_pub_actors, :uri, unique: true
    add_index :activity_pub_instances, :hostname
    add_index :activity_pub_objects, :uri
    add_index :activity_pub_actors, :uri
  end
end
