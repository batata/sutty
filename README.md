# Sutty

* [Castellano](#castellano)
* [English](#english)

## Castellano

Sutty es una plataforma para alojar sitios más seguros, veloces
y resilientes, orientada a organizaciones, colectivxs y activistas.
Está basada en el generador de sitios estáticos
[Jekyll](https://jekyllrb.com).

Este repositorio es la plataforma _Ruby on Rails_ para alojar el
[panel](https://panel.sutty.nl/) de autogestión de sitios.

Para más información visita el [sitio de Sutty](https://sutty.nl/).

### Desarrollar

Para facilitar la gestión de dependencias y entorno de desarrollo,
instala [haini.sh](https://0xacab.org/sutty/haini.sh)


Todas las tareas se gestionan con `go-task`.  [Instrucciones de
instalación (en inglés)](https://taskfile.dev/installation/)

```bash
go-task
```

### Variables de entorno

Las variables de entorno por defecto se encuentran en el archivo `.env`.
Para modificar las opciones, crear o modificar el archivo `.env.local`
con valores distintos.

### Documentación

Para navegar la documentación del código usando YARD:

```bash
go-task doc serve
```

Y luego navegar a <https://panel.sutty.local:3000/doc/>

## English

Sutty is a platform for hosting safer, faster and more resilient
websites, aimed at organizations, collectives and activists.  It's based
on [Jekyll](https://jekyllrb.com/), the static website generator.

This repository is the Ruby on Rails platform that hosts the
self-managed [panel](https://panel.sutty.nl/).

For more information, visit [Sutty's website](https://sutty.nl/en/).

### Development


To facilitate dependencies and dev environment, install
[haini.sh](https://0xacab.org/sutty/haini.sh)

Every task is run via `go-task`.  [Installation
instructions](https://taskfile.dev/installation/).

```bash
go-task
```

### Environment variables

Default env vars are store on `.env`.  For local options, copy them to
`.env.local`.

### Documentation

To browse documentation using YARD:

```bash
go-task doc serve
```

And then open <https://panel.sutty.local:3000/doc/>

