# frozen_string_literal: true

# Crea la tabla de diseños
class CreateDesigns < ActiveRecord::Migration[5.2]
  def change
    create_table :designs do |t|
      t.timestamps
      t.string :name, unique: true
      t.text   :description
      t.string :gem, unique: true
      t.string :url
      t.string :license
    end
  end
end
