# frozen_string_literal: true

# Notifica a les usuaries cuando un sitio se generó con éxito
#
# XXX: No será mejor enviarles un correo con copia?
# TODO: Agregar headers de desuscripción de notificaciones cuando
# tengamos opciones de usuarie
# TODO: Agregar firma GPG y header Autocrypt
# TODO: Cifrar con GPG si le usuarie nos dio su llave
class DeployMailer < ApplicationMailer
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::DateHelper

  # rubocop:disable Metrics/AbcSize
  def deployed(deploys = {})
    usuarie = params[:usuarie]
    hostname = site.hostname
    deploys ||= {}

    # Informamos a cada quien en su idioma y damos una dirección de
    # respuesta porque a veces les usuaries nos escriben
    I18n.with_locale(usuarie.lang) do
      subject = t('.subject', site: site.name)

      @hi = t('.hi')
      @explanation = t('.explanation', fqdn: hostname)
      @help = t('.help')

      @headers = %w[type status url seconds size].map do |header|
        t(".th.#{header}")
      end

      @table = deploys.each_pair.map do |deploy, value|
        {
          title: t(".#{deploy}.title"),
          status: t(".#{deploy}.#{value[:status] ? 'success' : 'error'}"),
          urls: value[:urls],
          seconds: {
            human: distance_of_time_in_words(value[:seconds].seconds),
            machine: "PT#{value[:seconds]}S"
          },
          size: number_to_human_size(value[:size], precision: 2)
        }
      end

      @terminal_table = Terminal::Table.new do |t|
        t << @headers
        t.add_separator
        @table.each do |row|
          row[:urls].each do |url|
            t << (row.map do |k, v|
              case k
              when :seconds then v[:human]
              when :urls then url.to_s
              else v
              end
            end)
          end
        end
      end

      mail(to: usuarie.email, reply_to: "sutty@#{Site.domain}", subject: subject)
    end
  end
  # rubocop:enable Metrics/AbcSize

  private

  def t(key, **args)
    I18n.t("deploy_mailer.deployed#{key}", **args)
  end
end
