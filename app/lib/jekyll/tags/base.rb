# frozen_string_literal: true

module Jekyll
  module Tags
    class Base < Liquid::Tag
      def render(context)
        context.registers[:site].config['url']
      end
    end
  end
end
