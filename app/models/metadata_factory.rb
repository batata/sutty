# frozen_string_literal: true

# Devuelve metadatos de cierto tipo
class MetadataFactory
  class << self
    def build(**args)
      classify(args[:type]).new(**args)
    end

    def classify(type)
      @factory_cache ||= {}
      @factory_cache[type] ||= ('Metadata' + type.to_s.camelcase).constantize
    end
  end
end
