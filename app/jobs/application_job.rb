# frozen_string_literal: true

# Base para trabajos
class ApplicationJob < ActiveJob::Base
  include Que::ActiveJob::JobExtensions

  # Esperar una cantidad random de segundos primos, para que no se
  # superpongan tareas
  #
  # @return [Array<Integer>]
  RANDOM_WAIT = [3, 5, 7, 11, 13].freeze

  # @return [ActiveSupport::Duration]
  def self.random_wait
    RANDOM_WAIT.sample.seconds
  end

  attr_reader :site

  # Si falla por cualquier cosa informar y descartar
  discard_on(Exception) do |job, error|
    ExceptionNotifier.notify_exception(error, data: { job: job })
  end
end
