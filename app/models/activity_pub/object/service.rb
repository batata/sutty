# frozen_string_literal: true

# = Service =
class ActivityPub
  class Object
    class Service < ActivityPub::Object
      include Concerns::ActorTypeConcern
    end
  end
end
