# frozen_string_literal: true

# Crea la tabla donde se indexa el contenido de los artículos, los
# IndexedPosts van a estar relacionados con un Post del mismo UUID.
#
# Solo contienen la información mínima necesaria para mostrar los
# resultados de búsqueda.
class CreateIndexedPosts < ActiveRecord::Migration[6.1]
  def change
    # Necesario para gen_random_uuid()
    #
    # XXX: En realidad no lo necesitamos porque cada IndexedPost va a
    # tener el UUID del Post correspondiente.
    enable_extension 'pgcrypto'

    create_table :indexed_posts, id: false do |t|
      t.primary_key :id, :uuid, default: 'public.gen_random_uuid()'
      t.belongs_to :site, index: true
      t.timestamps

      # Filtramos por idioma
      t.string :locale, default: 'simple', index: true
      # Vamos a querer filtrar por layout
      t.string :layout, null: false, index: true
      # Esta es la ruta al artículo
      t.string :path, null: false
      # Queremos mostrar el título por separado
      t.string :title, default: ''
      # También vamos a mostrar las categorías
      t.jsonb :front_matter, default: '{}'
      t.string :content, default: ''
      t.tsvector :indexed_content

      t.index :indexed_content, using: 'gin'
      t.index :front_matter, using: 'gin'
    end

    # Crea un trigger que actualiza el índice tsvector con el título y
    # contenido del artículo y su idioma.
    create_trigger(compatibility: 1).on(:indexed_posts).before(:insert, :update) do
      "new.indexed_content := to_tsvector(('pg_catalog.' || new.locale)::regconfig, coalesce(new.title, '') || '\n' || coalesce(new.content,''));"
    end
  end
end
