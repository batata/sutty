# frozen_string_literal: true

# Permite traer los cambios desde webhooks

class GitPullJob < ApplicationJob
  # @param :site [Site]
  # @param :usuarie [Usuarie]
  # @return [nil]
  def perform(site, usuarie)
    @site = site

    return unless site.repository.origin
    return unless site.repository.fetch.positive?

    site.repository.merge(usuarie)
    site.reindex_changes!
  end
end
