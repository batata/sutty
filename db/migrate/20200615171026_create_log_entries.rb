# frozen_string_literal: true

class CreateLogEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :log_entries do |t|
      t.timestamps
      t.belongs_to :site, index: true
      t.text :text
    end
  end
end
