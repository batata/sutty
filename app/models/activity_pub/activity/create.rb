# frozen_string_literal: true

class ActivityPub
  class Activity
    class Create < ActivityPub::Activity; end
  end
end
