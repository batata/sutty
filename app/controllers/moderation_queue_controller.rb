# frozen_string_literal: true

# Cola de moderación de ActivityPub
class ModerationQueueController < ApplicationController
  include ModerationFiltersConcern

  before_action :authenticate_usuarie!

  breadcrumb -> { current_usuarie.email }, :edit_usuarie_registration_path
  breadcrumb 'sites.index', :sites_path, match: :exact

  # Cola de moderación viendo todo el sitio
  def index
    authorize ModerationQueue.new(site)
    breadcrumb site.title, site_posts_path(site)
    breadcrumb I18n.t('moderation_queue.index.title'), ''

    site.moderation_checked!

    # @todo cambiar el estado por query
    @activity_pubs = site.activity_pubs
    @instance_moderations = rubanok_process(site.instance_moderations, with: InstanceModerationProcessor)
    @actor_moderations = rubanok_process(site.actor_moderations, with: ActorModerationProcessor)
    @moderation_queue = rubanok_process(site.activity_pubs, with: ActivityPubProcessor)
  end
end
