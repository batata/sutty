# frozen_string_literal: true

# Guarda la mención en la tabla de actores
class AddMentionToActors < ActiveRecord::Migration[6.1]
  def up
    add_column :activity_pub_actors, :mention, :string, null: true

    actor_types = %w[
      ActivityPub::Object::Application
      ActivityPub::Object::Group
      ActivityPub::Object::Organization
      ActivityPub::Object::Person
      ActivityPub::Object::Service
    ]

    ActivityPub::Object.where(type: actor_types).where.not(content: {}).find_each do |object|
      ActivityPub::Actor.find_by_uri(object.uri)&.save
    end
  end

  def down
    remove_column :activity_pub_actors, :mention
  end
end
