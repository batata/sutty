# frozen_string_literal: true

# El diseño de un sitio es la plantilla/tema.  En este modelo cargamos
# las propiedades para poder verlas desde el panel y elegir un diseño
# para el sitio.
#
# TODO: Agregar captura de pantalla con ActiveStorage
class Design < ApplicationRecord
  extend Mobility

  NO_THEMES = %w[sutty-theme-none sutty-theme-custom].freeze

  translates :name, type: :string, locale_accessors: true
  translates :description, type: :text, locale_accessors: true
  translates :credits, type: :text, locale_accessors: true

  has_many :sites

  validates :name, presence: true, uniqueness: true
  validates :gem, presence: true, uniqueness: true
  validates :description, presence: true

  def no_theme?
    NO_THEMES.include? gem
  end
end
