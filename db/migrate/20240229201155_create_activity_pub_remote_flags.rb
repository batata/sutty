# frozen_string_literal: true

# Lleva el registro de reportes remotos
class CreateActivityPubRemoteFlags < ActiveRecord::Migration[6.1]
  def change
    create_table :activity_pub_remote_flags, id: :uuid do |t|
      t.timestamps
      t.belongs_to :site
      t.uuid :actor_id, index: true

      t.text :message

      t.index %i[site_id actor_id], unique: true
    end
  end
end
