# frozen_string_literal: true

# A veces tenemos varias acciones sobre el mismo objeto
class RemoveUniqueIndexFromActivityPubs < ActiveRecord::Migration[6.1]
  def change
    remove_index :activity_pubs, %i[site_id object_id object_type], unique: true
  end
end
