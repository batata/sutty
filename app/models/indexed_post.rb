# frozen_string_literal: true

# La representación indexable de un artículo
class IndexedPost < ApplicationRecord
  include PgSearch::Model

  # La traducción del locale según Sutty al locale según PostgreSQL
  DICTIONARIES = {
    es: 'spanish',
    en: 'english'
  }.freeze

  # TODO: Los indexed posts tienen que estar scopeados al idioma actual,
  # no buscar sobre todos
  pg_search_scope :search,
                  lambda { |locale, query|
                    {
                      against: :content,
                      query: query,
                      using: {
                        tsearch: {
                          dictionary: IndexedPost.to_dictionary(locale: locale),
                          tsvector_column: 'indexed_content'
                        },
                        trigram: {
                          word_similarity: true
                        }
                      }
                    }
                  }

  # Trae los IndexedPost en el orden en que van a terminar en el sitio.
  default_scope -> { order(order: :desc, created_at: :desc) }
  scope :in_category, ->(category) { where("front_matter->'categories' ? :category", category: category.to_s) }
  scope :by_usuarie, ->(usuarie) { where("front_matter->'usuaries' @> :usuarie::jsonb", usuarie: usuarie.to_s) }

  belongs_to :site

  # Encuentra el post original
  #
  # @return [nil,Post]
  def post
    return if post_id.blank?

    @post ||= site.posts(lang: locale).find(post_id, uuid: true)
  end

  # Convertir locale a direccionario de PG
  #
  # @param [String,Symbol]
  # @return [String]
  def self.to_dictionary(locale:)
    DICTIONARIES[locale.to_sym] || 'simple'
  end
end
