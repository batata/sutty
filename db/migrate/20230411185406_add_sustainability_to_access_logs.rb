# frozen_string_literal: true

# Agrega las columnas de calculo de emisiones de CO2
class AddSustainabilityToAccessLogs < ActiveRecord::Migration[6.1]
  def change
    %i[datacenter_co2 network_co2 consumer_device_co2 production_co2 total_co2].each do |column|
      add_column :access_logs, column, :decimal, limit: 53
    end
  end
end
