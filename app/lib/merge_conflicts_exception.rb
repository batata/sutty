# frozen_string_literal: true

class MergeConflictsException < RuntimeError; end
