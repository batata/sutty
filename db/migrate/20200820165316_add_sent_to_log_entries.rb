class AddSentToLogEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :log_entries, :sent, :boolean, default: false
  end
end
