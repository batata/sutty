# frozen_string_literal: true

# La recolección de estadísticas podría pertenecer a un sitio
class AddSiteToStats < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :stats, :site, index: true, null: true
  end
end
