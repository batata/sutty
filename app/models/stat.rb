# frozen_string_literal: true

# Registran cuándo fue la última recolección de datos.
class Stat < ApplicationRecord
  # XXX: Los intervalos van en orden de mayor especificidad a menor
  INTERVALS = %i[day].freeze
  RESOURCES = %i[builds space_used build_time].freeze
  COLUMNS = %i[http_referer geoip2_data_country_name].freeze

  belongs_to :site

  # El intervalo por defecto
  #
  # @return [Symbol]
  def self.default_interval
    INTERVALS.first
  end
end
