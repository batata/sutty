# frozen_string_literal: true

# = Place =
#
# Representa artículos
class ActivityPub
  class Object
    class Place < ActivityPub::Object; end
  end
end
