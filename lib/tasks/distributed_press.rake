# frozen_string_literal: true

namespace :distributed_press do
  namespace :tokens do
    desc 'Renew tokens'
    task renew: :environment do
      RenewDistributedPressTokensJob.perform_now
    end
  end
end
