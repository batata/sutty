# frozen_string_literal: true

require 'distributed_press/v1/social/client'
require 'distributed_press/v1/social/allowlist'
require 'distributed_press/v1/social/blocklist'
require 'distributed_press/v1/social/hook'
require 'distributed_press/v1/social/inbox'
require 'distributed_press/v1/social/dereferencer'
require 'httparty/cache/store/redis'

# Gestiona la Social Inbox de un sitio
class SocialInbox
  # @return [Site]
  attr_reader :site

  # @param :site [Site]
  def initialize(site:)
    @site = site
  end

  # @return [String]
  def actor
    @actor ||=
      begin
        user   = site.config.dig('activity_pub', 'username')
        user ||= hostname.split('.', 2).first

        "@#{user}@#{hostname}"
      end
  end

  def actor_id
    @actor_id ||= SocialInbox.generate_uri(hostname) do |uri|
      uri.path = '/about.jsonld'
    end
  end

  # @return [DistributedPress::V1::Social::Client]
  def client
    @client ||= client_for site.config.dig('activity_pub', 'url')
  end

  # Permite enviar mensajes directo a otro servidor
  #
  # @param url [String]
  # @return [DistributedPress::V1::Social::Client]
  def client_for(url)
    raise 'Falló generar un cliente' if url.blank?

    @client_for ||= {}
    @client_for[url] ||=
      DistributedPress::V1::Social::Client.new(
        url: url,
        public_key_url: public_key_url,
        private_key_pem: site.private_key_pem,
        logger: Rails.logger,
        cache_store: HTTParty::Cache::Store::Redis.new(redis_url: ENV.fetch('REDIS_SERVER', nil))
      )
  end

  # @return [DistributedPress::V1::Social::Inbox]
  def inbox
    @inbox ||= DistributedPress::V1::Social::Inbox.new(client: client, actor: actor)
  end

  # @return [DistributedPress::V1::Social::Dereferencer]
  def dereferencer
    @dereferencer ||= DistributedPress::V1::Social::Dereferencer.new(client: client)
  end

  # @return [DistributedPress::V1::Social::Hook]
  def hook
    @hook ||= DistributedPress::V1::Social::Hook.new(client: client, actor: actor)
  end

  # @return [DistributedPress::V1::Social::Allowlist]
  def allowlist
    @allowlist ||= DistributedPress::V1::Social::Allowlist.new(client: client, actor: actor)
  end

  # @return [DistributedPress::V1::Social::Blocklist]
  def blocklist
    @blocklist ||= DistributedPress::V1::Social::Blocklist.new(client: client, actor: actor)
  end

  # @return [String]
  def public_key_url
    @public_key_url ||= SocialInbox.generate_uri(hostname) do |uri|
      uri.path = '/about.jsonld'
      uri.fragment = 'main-key'
    end
  end

  # El hostname puede estar en varios lados...
  #
  # @return [String]
  def hostname
    @hostname ||=
      site.config.dig('activity_pub', 'hostname') || site.config['hostname'] || site.hostname
  end

  # Genera una URI dentro de este sitio
  #
  # @return [String]
  def self.generate_uri(hostname, &block)
    URI("https://#{hostname}").tap(&block).to_s
  end
end
