# frozen_string_literal: true

# = Generic =
class ActivityPub
  class Object
    class Generic < ActivityPub::Object; end
  end
end
