# frozen_string_literal: true

module ActiveStorage
  # Modificar {DiskController} para poder asociar el blob a un sitio
  module DiskControllerDecorator
    extend ActiveSupport::Concern

    included do
      alias_method :original_show, :show

      # Permitir incrustar archivos subidos (especialmente PDFs) desde
      # otros sitios.
      def show
        original_show.tap do |s|
          response.headers.delete 'X-Frame-Options'
        end
      end

      rescue_from ActiveRecord::RecordNotFound, with: :page_not_found

      # Asociar el archivo subido al sitio correspondiente.  Cada sitio
      # tiene su propio servicio de subida de archivos.
      def update
        if (token = decode_verified_token)
          if acceptable_content?(token)
            blob = ActiveStorage::Blob.find_by_key! token[:key]
            site = Site.find_by_name token[:service_name]

            if remote_file?(token)
              begin
                url = request.body.read
                body = Down.download(url, max_size: 111.megabytes)
                checksum = Digest::MD5.file(body.path).base64digest
                blob.metadata[:url] = url
                blob.update_columns checksum: checksum, byte_size: body.size, metadata: blob.metadata
              rescue StandardError => e
                ExceptionNotifier.notify_exception(e, data: { key: token[:key], url: url, site: site.name })

                head :content_too_large
              end
            else
              body = request.body
              checksum = token[:checksum]
            end

            named_disk_service(token[:service_name]).upload token[:key], body, checksum: checksum

            site.static_files.attach(blob)
          else
            head :unprocessable_entity
          end
        else
          head :not_found
        end
      rescue ActiveStorage::IntegrityError
        head :unprocessable_entity
      end

      private

      def remote_file?(token)
        token[:content_type] == 'sutty/download-from-url'
      end

      def page_not_found(exception)
        head :not_found
        ExceptionNotifier.notify_exception(exception, data: {params: params.to_hash})
      end
    end
  end
end

ActiveStorage::DiskController.include ActiveStorage::DiskControllerDecorator
