Color_Off='\e[0m'
BPurple='\e[1;35m'
BBlue='\e[1;34m'

is_git() {
  git rev-parse --abbrev-ref HEAD 2>/dev/null
}

PS1="\[${BPurple}\]\$(is_git) \[${BBlue}\]\W\[${Color_Off}\] >_ "
