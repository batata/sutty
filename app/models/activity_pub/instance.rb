# frozen_string_literal: true

# = Instance =
#
# Representa cada instancia del fediverso que interactúa con la Social
# Inbox.
class ActivityPub
  class Instance < ApplicationRecord
    include AASM

    validates :aasm_state, presence: true, inclusion: { in: %w[paused allowed blocked] }
    validates :hostname, uniqueness: true, hostname: { allow_numeric_hostname: true }

    has_many :activity_pubs
    has_many :actors
    has_many :instance_moderations

    # XXX: Mantenemos esto por si queremos bloquear una instancia a
    # nivel general
    aasm do
      state :paused, initial: true
      state :allowed
      state :blocked

      # Al pasar una instancia a bloqueo, quiere decir que todos los
      # sitios adoptan esta lista
      event :block do
        transitions from: %i[paused allowed], to: :blocked
      end
    end

    def list_name
      "@*@#{hostname}"
    end

    def uri
      @uri ||= "https://#{hostname}/"
    end
  end
end
