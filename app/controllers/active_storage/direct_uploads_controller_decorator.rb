# frozen_string_literal: true

module ActiveStorage
  # Modifica la creación de un blob antes de subir el archivo para que
  # incluya el JekyllService adecuado.
  module DirectUploadsControllerDecorator
    extend ActiveSupport::Concern

    included do
      def create
        blob = ActiveStorage::Blob.create_before_direct_upload!(service_name: session[:service_name], **blob_args)
        render json: direct_upload_json(blob)
      end

      private

      # Normalizar los caracteres unicode en los nombres de archivos
      # para que puedan propagarse correctamente a través de todo el
      # stack.
      def blob_args
        params.require(:blob).permit(:filename, :byte_size, :checksum, :content_type,
                                     metadata: {}).to_h.symbolize_keys.tap do |ba|
          ba[:filename] = ba[:filename].unicode_normalize.sub(/\A_+/, '')
        end
      end
    end
  end
end

ActiveStorage::DirectUploadsController.include ActiveStorage::DirectUploadsControllerDecorator
