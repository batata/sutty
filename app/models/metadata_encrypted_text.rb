# frozen_string_literal: true

# Como todos los campos pueden ser cifrados, forzamos el cifrado
# configurando este texto como privado.
#
# TODO: Deprecar
class MetadataEncryptedText < MetadataText
  def private?
    true
  end
end
