# frozen_string_literal: true

class AddAnonymousToSite < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :colaboracion_anonima, :boolean, default: false
  end
end
