# frozen_string_literal: true

# Integración con la tienda
module Tienda
  extend ActiveSupport::Concern

  included do
    has_encrypted :tienda_api_key

    def tienda?
      tienda_api_key.present? && tienda_url.present?
    end

    # Sugerir una dirección al configurar por primera vez
    def tienda_url
      t = read_attribute(:tienda_url)

      return t if new_record?

      t.blank? ? "https://#{name}.#{ENV.fetch('TIENDA', 'tienda.sutty.nl')}" : t
    end
  end
end
