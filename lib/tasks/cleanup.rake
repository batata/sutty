# frozen_string_literal: true

namespace :cleanup do
  desc 'Cleanup sites'
  task everything: :environment do |_, args|
    before = ENV.fetch('BEFORE', '30').to_i.days.ago

    CleanupJob.perform_later(before)
  end
end
