# frozen_string_literal: true

module ExceptionNotifier
  # Notifica las excepciones como incidencias en Gitlab
  class GitlabNotifier
    def initialize(_); end

    # Recibe la excepción y empieza la tarea de notificación en segundo
    # plano.
    #
    # @param :exception [Exception]
    # @param :options [Hash]
    def call(exception, options, &block)
      case exception
      when BacktraceJob::BacktraceException
        GitlabNotifierJob.perform_later(exception, **options)
      else
        GitlabNotifierJob.perform_now(exception, **options)
      end
    end
  end
end
