class ChangeBytesToBigInt < ActiveRecord::Migration[6.1]
  def change
    change_column :build_stats, :bytes, :bigint
  end
end
