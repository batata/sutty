# frozen_string_literal: true

# Un campo de orden
class MetadataOrder < MetadataTemplate
  # El valor según la posición del post en la relación ordenada por
  # fecha, a fecha más alta, posición más alta
  def default_value
    super || site.posts(lang: lang).sort_by(:date).index(post)
  end

  def save
    return true unless changed?

    self[:value] = value.to_i

    true
  end

  # El orden nunca puede ser privado
  def private?
    false
  end
end
