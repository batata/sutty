# frozen_string_literal: true

require 'test_helper'

class IndexedPostTest < ActiveSupport::TestCase
  def site
    @site ||= create :site
  end

  teardown do
    @site&.destroy
  end

  test 'se pueden convertir los diccionarios' do
    IndexedPost::DICTIONARIES.each do |locale, dict|
      assert_equal dict, IndexedPost.to_dictionary(locale: locale)
    end
  end

  test 'se pueden buscar por categoría' do
    assert(post = site.posts.create(title: SecureRandom.hex, description: SecureRandom.hex,
                                    categories: [SecureRandom.hex, SecureRandom.hex]))
    assert_not_empty site.indexed_posts.in_category(post.categories.value.sample)
  end

  test 'se pueden encontrar por usuarie' do
    usuarie = create :usuarie
    assert(post = site.posts.create(title: SecureRandom.hex, description: SecureRandom.hex))

    post.usuaries << usuarie
    post.save

    assert_not_empty site.indexed_posts.by_usuarie(usuarie.id)
  end
end
