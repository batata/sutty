# frozen_string_literal: true

module Jekyll
  module Tags
    # Genera un tag vacío que sirve para reemplazar tags provistos por
    # complementos que no vamos a cargar dentro del panel (seo, feed,
    # etc.)  Lo correcto sería modificar Liquid::Document para que
    # ignore los tags desconocidos, pero en nuestras pruebas los toma
    # como el comienzo de un bloque e ignora HTML adyacente, así que
    # preferimos avanzar con una lista predeterminada.
    #
    # @see config/initializers/core_extensions.rb
    class Empty < Liquid::Tag
      def render(_)
        ''
      end
    end
  end
end
