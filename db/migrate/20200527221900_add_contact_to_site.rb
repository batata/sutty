# frozen_string_literal: true

# Los sitios pueden tener un formulario de contacto.  Pueden
# deshabilitarlo si están recibiendo spam o un ataque.
class AddContactToSite < ActiveRecord::Migration[6.0]
  def change
    add_column :sites, :contact, :boolean, default: false
  end
end
