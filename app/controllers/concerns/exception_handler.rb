# frozen_string_literal: true

# Gestiona las excepciones que lanzamos desde los controladores
module ExceptionHandler
  extend ActiveSupport::Concern

  class SiteNotFound < StandardError; end
  class PageNotFound < StandardError; end

  included do
    rescue_from SiteNotFound, with: :site_not_found
    rescue_from PageNotFound, with: :page_not_found
    rescue_from ActionController::RoutingError, with: :page_not_found
    rescue_from Pundit::NilPolicyError, with: :page_not_found
    rescue_from Pundit::NilPolicyError, with: :page_not_found
    rescue_from ActionController::RoutingError, with: :page_not_found
    rescue_from ActionController::ParameterMissing, with: :page_not_found
  end

  def site_not_found
    reset_response!

    flash[:error] = I18n.t('errors.site_not_found')

    redirect_to sites_path
  end

  def page_not_found
    reset_response!

    render 'application/page_not_found', status: :not_found
  end

  private

  def reset_response!
    self.response_body = nil
    @_response_body = nil

    headers.delete('Location')
  end
end
