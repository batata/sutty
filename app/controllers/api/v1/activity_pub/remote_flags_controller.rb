# frozen_string_literal: true

module Api
  module V1
    module ActivityPub
      # Devuelve los reportes remotos hechos
      #
      # @todo Verificar la firma. Por ahora no es necesario porque no es
      # posible obtener remotamente todos los reportes y se identifican por
      # UUIDv4.
      class RemoteFlagsController < BaseController
        skip_forgery_protection

        def show
          render json: (remote_flag&.content || {}), content_type: 'application/activity+json'
        end

        private

        # @return [ActivityPub::RemoteFlag,nil]
        def remote_flag
          @remote_flag ||= ::ActivityPub::RemoteFlag.find(params[:id])
        end
      end
    end
  end
end
