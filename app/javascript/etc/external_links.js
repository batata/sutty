document.addEventListener('turbolinks:load', () => {
  document.querySelectorAll("a[href^='http://'],a[href^='https://'],a[href^='//']").forEach(a => {
    a.rel = "noopener"
    a.target = "_blank"
  })
})
