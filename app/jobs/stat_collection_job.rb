# frozen_string_literal: true

# Genera resúmenes de información para poder mostrar estadísticas y se
# corre regularmente a sí misma.
class StatCollectionJob < PeriodicJob
  include RecursiveRollup

  STAT_NAME = 'stat_collection_job'

  def perform(site:, once: true)
    @site = site
    beginning = beginning_of_interval
    stat = site.stats.create! name: STAT_NAME

    scope.rollup('builds', **options)

    scope.rollup('space_used', **options) do |rollup|
      rollup.average(:bytes)
    end

    scope.rollup('build_time', **options) do |rollup|
      rollup.average(:seconds)
    end

    dimensions = { site_id: site.id }

    reduce_rollup(name: 'builds', operation: :sum, dimensions: dimensions)
    reduce_rollup(name: 'space_used', operation: :average, dimensions: dimensions)
    reduce_rollup(name: 'build_time', operation: :average, dimensions: dimensions)

    stat.touch
    run_again! unless once
  end

  private

  # Los registros a procesar
  #
  # @return [ActiveRecord::Relation]
  def scope
    @scope ||= site.build_stats.jekyll.where('build_stats.created_at >= ?', beginning_of_interval).group(:site_id)
  end

  # Las opciones por defecto
  #
  # @return [Hash]
  def options
    @options ||= { interval: starting_interval, update: true }
  end

  def stat_name
    STAT_NAME
  end
end
