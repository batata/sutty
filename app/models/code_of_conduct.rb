# frozen_string_literal: true

# Códigos de conducta
class CodeOfConduct < ApplicationRecord
  extend Mobility

  translates :title, type: :string, locale_accessors: true
  translates :description, type: :text, locale_accessors: true
  translates :content, type: :text, locale_accessors: true

  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
  validates :content, presence: true
end
