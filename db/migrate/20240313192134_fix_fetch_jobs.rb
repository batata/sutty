# frozen_string_literal: true

class FixFetchJobs < ActiveRecord::Migration[6.1]
  def up
    QueJob.where("last_error_message like '%ActiveJob::DeserializationError%'").find_each do |job|
      job.error_count = 0
      job.run_at = Time.now

      job.args.first['arguments'].first['_aj_ruby2_keywords'].delete('object')
      job.args.first['arguments'].first['_aj_ruby2_keywords'] << 'object_id'

      object = job.args.first['arguments'].first.delete('object')['_aj_globalid']
      job.args.first['arguments'].first['object_id'] = object.split('/').last

      job.save
    end
  end

  def down; end
end
