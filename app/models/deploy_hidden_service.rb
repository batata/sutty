# frozen_string_literal: true

# Genera una versión onion
class DeployHiddenService < DeployWww
  store :values, accessors: %i[onion], coder: JSON

  before_create :create_hidden_service!

  ONION_RE = /\A[a-z0-9]{56}\.onion\z/.freeze

  def fqdn
    create_hidden_service! if onion.blank?

    onion.tap do |onion|
      raise ArgumentError, 'Aun no se generó la dirección .onion' if onion.blank?
    end
  end

  def url
    "http://#{fqdn}"
  end

  private

  def create_hidden_service!
    onion_address = HiddenServiceClient.new.create(site.name)

    if ONION_RE =~ onion_address
      self.onion = onion_address

      usuarie = GitAuthor.new email: "tor@#{Site.domain}", name: 'Tor'
      params = { onion: onion_address, deploy: self }

      SiteService.new(site: site, usuarie: usuarie, params: params).add_onion
    end
  end
end
