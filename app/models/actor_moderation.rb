# frozen_string_literal: true

# Mantiene la relación entre Site y Actor
class ActorModeration < ApplicationRecord
  IGNORED_EVENTS = %i[remove].freeze
  IGNORED_STATES = %i[removed].freeze

  include AASM

  belongs_to :site
  belongs_to :remote_flag, optional: true, class_name: 'ActivityPub::RemoteFlag'
  belongs_to :actor, class_name: 'ActivityPub::Actor'

  accepts_nested_attributes_for :remote_flag

  aasm do
    state :paused, initial: true
    state :allowed
    state :blocked
    state :reported
    state :removed

    error_on_all_events do |e|
      ExceptionNotifier.notify_exception(e, data: { site: site.name, actor: actor.uri, actor_moderation: id })
    end

    event :pause do
      transitions from: %i[allowed blocked reported], to: :paused, after: :synchronize!
    end

    # Al permitir una cuenta no se permiten todos los comentarios
    # pendientes de moderación que ya hizo.
    event :allow do
      transitions from: %i[paused blocked reported], to: :allowed, after: :synchronize!
    end

    # Al bloquear una cuenta no se bloquean todos los comentarios
    # pendientes de moderación que hizo.
    event :block do
      transitions from: %i[paused allowed], to: :blocked, after: :synchronize!
    end

    # Al reportar, necesitamos asociar una RemoteFlag para poder
    # enviarla.
    event :report do
      transitions from: %i[pause allowed blocked], to: :reported, after: :synchronize!

      after do
        ActivityPub::RemoteFlagJob.perform_later(remote_flag: remote_flag) if remote_flag.waiting?
      end
    end

    # Si un perfil es eliminado remotamente, tenemos que dejar de
    # mostrarlo y todas sus actividades.
    event :remove do
      transitions to: :removed

      after do
        site.activity_pubs.where(actor_id: actor_id).remove_all!
      end
    end
  end

  # Definir eventos en masa
  include AasmEventsConcern

  def synchronize!
    ActivityPub::SyncListsJob.perform_later(site: site)
  end
end
