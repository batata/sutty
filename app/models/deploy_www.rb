# frozen_string_literal: true

# Vincula la versión del sitio con www a la versión sin
class DeployWww < Deploy
  store :values, accessors: %i[], coder: JSON

  DEPENDENCIES = %i[deploy_local]

  before_destroy :remove_destination!

  def deploy(output: false)
    puts "Creando symlink #{site.hostname} => #{destination}" if output

    File.symlink?(destination) ||
      File.symlink(site.hostname, destination).zero?
  end

  def limit
    1
  end

  def size
    File.size destination
  end

  def destination
    File.join(Rails.root, '_deploy', fqdn)
  end

  def fqdn
    "www.#{site.hostname}"
  end

  def url
    "https://#{fqdn}/"
  end

  private

  def remove_destination!
    FileUtils.rm_f destination
  end
end
