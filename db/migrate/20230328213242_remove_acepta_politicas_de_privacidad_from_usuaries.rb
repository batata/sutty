# frozen_string_literal: true

# Elimina un campo que nunca se usó
class RemoveAceptaPoliticasDePrivacidadFromUsuaries < ActiveRecord::Migration[6.1]
  def change
    remove_column :usuaries, :acepta_politicas_de_privacidad, :boolean, default: false
  end
end
