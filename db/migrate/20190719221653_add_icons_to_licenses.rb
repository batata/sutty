# frozen_string_literal: true

# Agrega íconos a las licencias
class AddIconsToLicenses < ActiveRecord::Migration[5.2]
  def change
    # XXX: Cambiar por ActiveStorage?
    add_column :licencias, :icons, :string
  end
end
