# frozen_string_literal: true

# Nos movemos de HABTM a una tabla de roles que nos permite tener otras
# características.
#
# No usamos ninguna feature de los modelos porque podemos haber borrado
# código.  No queremos tener migraciones que luego no funcionen.
class MigrateFromHabtmToRoles < ActiveRecord::Migration[5.2]
  def up
    Site.all.each do |site|
      %w[usuarie invitade].each do |rol|
        table_name = rol.pluralize
        join_table = ['sites', table_name].sort.join('_')

        usuaries = Usuarie.find_by_sql("select usuaries.*
        from usuaries
        inner join #{join_table}
          on #{join_table}.usuarie_id = usuaries.id
        where #{join_table}.site_id = #{site.id}")

        usuaries.each do |usuarie|
          site.roles << Rol.create(site: site, usuarie: usuarie,
                                   temporal: false, rol: rol)
        end
      end
    end

    drop_table :sites_usuaries
    drop_table :invitades_sites
  end

  def down
    %i[sites_usuaries invitades_sites].each do |table|
      create_table table, id: false do |t|
        t.belongs_to :site
        t.belongs_to :usuarie
        t.index %i[site_id usuarie_id], unique: true
      end
    end

    Rol.all.each do |rol|
      table_name = ['sites', rol.rol.pluralize].sort.join('_')

      sql = "insert into #{table_name} (site_id, usuarie_id)
        values (#{rol.site_id}, #{rol.usuarie_id});"

      ActiveRecord::Base.connection.execute(sql)
    end

    drop_table :roles
  end
end
