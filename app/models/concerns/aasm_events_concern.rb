# frozen_string_literal: true

module AasmEventsConcern
  extend ActiveSupport::Concern

  included do
    # Todos los eventos de la máquina de estados
    #
    # @return [Array<Symbol>]
    def self.events
      aasm.events.map(&:name) - self::IGNORED_EVENTS
    end

    # Encuentra todos los eventos que se pueden ejecutar con el filtro
    # actual.
    #
    # @return [Array<Symbol>]
    def self.transitionable_events(current_state)
      events.select do |event|
        aasm.events.find { |x| x.name == event }.transitions_from_state? current_state
      end
    end

    # Todos los estados de la máquina de estados
    #
    # @return [Array<Symbol>]
    def self.states
      aasm.states.map(&:name) - self::IGNORED_STATES
    end

    # Define un método que cambia el estado para todos los objetos del
    # scope actual.
    #
    # @return [Bool] Si hubo al menos un error, devuelve false.
    aasm.events.map(&:name).each do |event|
      define_singleton_method(:"#{event}_all!") do
        successes = []

        find_each do |object|
          successes << (object.public_send(:"may_#{event}?") && object.public_send(:"#{event}!"))
        end

        successes.all?
      end

      # Ejecuta la transición del evento en la base de datos sin
      # ejecutar los callbacks, sin modificar los items del scope que no
      # pueden transicionar.
      #
      # @return [Integer] Registros modificados
      define_singleton_method(:"#{event}_all_without_callbacks!") do
        aasm_event = aasm.events.find { |e| e.name == event }
        to_state = aasm_event.transitions.map(&:to).first
        from_states = aasm_event.transitions.map(&:from)

        unscope(where: :aasm_state).where(aasm_state: from_states).update_all(aasm_state: to_state,
                                                                              updated_at: Time.now)
      end
    end
  end
end
