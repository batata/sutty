# frozen_string_literal: true

# = Profile =
#
# Representa artículos
class ActivityPub
  class Object
    class Profile < ActivityPub::Object; end
  end
end
