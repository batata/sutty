# frozen_string_literal: true

# El status de un sitio
class AddStatusToSite < ActiveRecord::Migration[5.2]
  def change
    add_column :sites, :status, :string, default: 'waiting'
  end
end
