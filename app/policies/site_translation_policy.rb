# frozen_string_literal: true

# Política de acceso a la traducción del sitio.
class SiteTranslationPolicy
  attr_reader :usuarie, :i18n

  def initialize(usuarie, i18n)
    @usuarie = usuarie
    @i18n    = i18n
  end

  # Solo las usuarias
  def index?
    !i18n.site.invitade?(usuarie)
  end

  def edit?
    index?
  end

  def update?
    index?
  end
end
