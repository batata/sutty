# frozen_string_literal: true

require 'test_helper'

class Post::IndexableTest < ActiveSupport::TestCase
  setup do
    @site = create :site
  end

  teardown do
    @site&.destroy
  end

  test 'los posts se indexan apenas se crean' do
    post = @site.posts.create(title: SecureRandom.hex, description: SecureRandom.hex)
    indexed_post = @site.indexed_posts.find_by_title post.title.value

    assert indexed_post
    assert_equal post.locale.value.to_s, indexed_post.locale
    assert_equal post.order.value, indexed_post.order
    assert_equal post.path.basename, indexed_post.path
    assert_equal post.layout.name.to_s, indexed_post.layout
  end

  test 'se pueden encontrar posts' do
    post = @site.posts.sample

    assert @site.indexed_posts.where(locale: post.lang.value).search(post.lang.value, post.title.value)
    assert @site.indexed_posts.where(locale: post.lang.value).search(post.lang.value, post.description.value)
  end

  test 'se pueden actualizar posts' do
    post = @site.posts.sample
    post.description.value = SecureRandom.hex

    assert post.save
    assert @site.indexed_posts.where(locale: post.lang.value).search(post.lang.value, post.description.value)
  end

  test 'al borrar el post se borra el indice' do
    post = @site.posts.sample
    assert post.destroy
    assert_not @site.indexed_posts.find_by_id(post.uuid.value)
  end
end
