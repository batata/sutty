# frozen_string_literal: true

# Mover todes les actores eliminades
class RemoveActorModerations < ActiveRecord::Migration[6.1]
  def up
    actor_ids =
      ActivityPub.where(aasm_state: 'removed', object_type: 'ActivityPub::Object::Person').distinct.pluck(:actor_id)

    ActorModeration.where(actor_id: actor_ids).remove_all!
  end

  def down; end
end
