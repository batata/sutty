# frozen_string_literal: true

# = Audio =
#
# Representa artículos
class ActivityPub
  class Object
    class Audio < ActivityPub::Object; end
  end
end
