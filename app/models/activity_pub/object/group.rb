# frozen_string_literal: true

# = Group =
class ActivityPub
  class Object
    class Group < ActivityPub::Object
      include Concerns::ActorTypeConcern
    end
  end
end
