# frozen_string_literal: true

# Cambia los msec a datetime para poder agregar por tiempos
class AddCreateAtToAccessLogs < ActiveRecord::Migration[6.1]
  def up
    add_column :access_logs, :created_at, :datetime, precision: 6

    create_trigger(compatibility: 1).on(:access_logs).before(:insert) do
      'new.created_at := to_timestamp(new.msec)'
    end

    ActiveRecord::Base.connection.execute('update access_logs set created_at = to_timestamp(msec);')
  end

  def down
    remove_column :access_logs, :created_at
  end
end
