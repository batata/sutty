# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class PostsControllerTest < ActionDispatch::IntegrationTest
      setup do
        @rol = create :rol
        @site = @rol.site
        @usuarie = @rol.usuarie

        @site.update_attribute :colaboracion_anonima, true
      end

      teardown do
        @site.destroy
      end

      test 'no se pueden enviar sin cookie' do
        post v1_site_posts_url(@site.hostname, layout: :post, **@host), params: {
          post: {
            title: SecureRandom.hex,
            description: SecureRandom.hex
          }
        }

        posts = @site.posts.size
        @site = Site.find(@site.id)

        assert_equal posts, @site.posts.size
        assert_response :precondition_required
        assert_equal 'expired_or_invalid_cookie', response.body
      end

      test 'no se pueden enviar a sitios que no existen' do
        site = SecureRandom.hex

        get v1_site_invitades_cookie_url(site_id: site, **@host)

        assert_not cookies[site]

        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        assert cookies[@site.name]

        post v1_site_posts_url(site_id: site, layout: :post, **@host),
             headers: { cookies: cookies },
             params: {
               consent: true,
               title: SecureRandom.hex,
               description: SecureRandom.hex
             }

        assert_response :precondition_required
        # XXX: Como la cookie es lo primero que se verifica, si el sitio
        # no existe tampoco se va a encontrar la cookie correcta.
        assert_equal 'expired_or_invalid_cookie', response.body
      end

      test 'antes hay que pedir una cookie' do
        assert_equal 2, @site.posts.size

        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        post v1_site_posts_url(@site.hostname, layout: :post, **@host),
             headers: {
               cookies: cookies,
               origin: @site.url
             },
             params: {
               consent: true,
               title: SecureRandom.hex,
               description: SecureRandom.hex
             }

        @site.reload

        assert_equal 3, @site.posts.size
        assert_response :redirect
      end

      test 'no se pueden enviar algunos valores' do
        uuid = SecureRandom.uuid
        date = Date.today + 2.days
        slug = SecureRandom.hex
        desc = SecureRandom.hex
        title = SecureRandom.hex
        order = (rand * 100).to_i

        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        post v1_site_posts_url(@site.hostname, layout: :post, **@host),
             headers: {
               cookies: cookies,
               origin: @site.url
             },
             params: {
               consent: true,
               title: title,
               description: desc,
               uuid: uuid,
               date: date,
               slug: slug,
               order: order
             }

        @site.reload
        p = @site.posts.find_by title: title

        assert p
        assert_equal desc, p.description.value
        assert_not_equal uuid, p.uuid.value
        assert_not_equal slug, p.slug.value
        assert_not_equal order, p.order.value
        assert_not_equal date, p.date.value
      end

      test 'las cookies tienen un vencimiento interno' do
        assert_equal 2, @site.posts.size

        get v1_site_invitades_cookie_url(@site.hostname, **@host)

        expired = (ENV.fetch('COOKIE_DURATION', '30').to_i + 1).minutes

        Timecop.freeze(Time.now + expired) do
          post v1_site_posts_url(@site.hostname, layout: :post, **@host),
               headers: {
                 cookies: cookies,
                 origin: @site.url
               },
               params: {
                 post: {
                   title: SecureRandom.hex,
                   description: SecureRandom.hex
                 }
               }
        end

        @site.reload

        assert_response :precondition_required
        assert_equal 2, @site.posts.size
        assert_equal 'expired_or_invalid_cookie', response.body
      end
    end
  end
end
