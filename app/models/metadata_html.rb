# frozen_string_literal: true

# Campos en HTML
class MetadataHtml < MetadataContent
  def front_matter?
    true
  end

  def document_value
    document.data[name.to_s]
  end
end
