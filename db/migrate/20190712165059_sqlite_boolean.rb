# frozen_string_literal: true

# Convertir los valores binarios de sqlite
class SqliteBoolean < ActiveRecord::Migration[5.2]
  def up
    return unless adapter_name == 'SQLite'

    Usuarie.where("acepta_politicas_de_privacidad = 't'")
           .update_all(acepta_politicas_de_privacidad: 1)
    Usuarie.where("acepta_politicas_de_privacidad = 'f'")
           .update_all(acepta_politicas_de_privacidad: 0)

    change_column :usuaries, :acepta_politicas_de_privacidad, :boolean,
                  default: 0
  end

  def down
    return unless adapter_name == 'SQLite'

    Usuarie.where('acepta_politicas_de_privacidad = 1')
           .update_all(acepta_politicas_de_privacidad: 't')
    Usuarie.where('acepta_politicas_de_privacidad = 0')
           .update_all(acepta_politicas_de_privacidad: 'f')

    change_column :usuaries, :acepta_politicas_de_privacidad, :boolean,
                  default: 'f'
  end
end
