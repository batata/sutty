# frozen_string_literal: true

# Estado de las listas de bloqueo en cada sitio
class FediblockStatesController < ApplicationController
  # Realiza cambios en las listas de bloqueo
  def action_on_several
    # Encontrar todas y deshabilitar las que no se enviaron
    site.fediblock_states.all.find_each do |fediblock_state|
      if fediblock_states_ids.include? fediblock_state.id
        fediblock_state.enable! if fediblock_state.may_enable?
      elsif fediblock_state.may_disable?
        fediblock_state.disable!
      end

      flash[:success] = I18n.t('fediblock_states.action_on_several.success')
    rescue Exception => e
      ExceptionNotifier.notify_exception(e, data: { site: site.name })

      flash.delete(:success)
      flash[:error] = I18n.t('fediblock_states.action_on_several.error')
    end

    # Bloquear otras instancias
    if custom_blocklist.present?
      if ActivityPub::InstanceModerationJob.perform_now(site: site, hostnames: custom_blocklist)
        flash[:success] = I18n.t('fediblock_states.action_on_several.custom_blocklist_success')
      else
        flash[:error] = I18n.t('fediblock_states.action_on_several.custom_blocklist_error')
      end
    end

    redirect_to site_moderation_queue_path
  end

  private

  def fediblock_states_ids
    params[:fediblock_states_ids] || []
  end

  # La lista de hostnames
  def custom_blocklist
    @custom_blocklist ||= fediblocks_states_params[:custom_blocklist].split("\n").map(&:strip).select(&:present?)
  end

  def fediblocks_states_params
    @fediblocks_states_params ||= params.permit(:custom_blocklist, fediblock_states_ids: [])
  end
end
