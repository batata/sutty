# frozen_string_literal: true

# Actualiza la relación entre un sitio y una instancia
class InstanceModerationsController < ApplicationController
  include ModerationConcern

  InstanceModeration.events.each do |event|
    define_method(event) do
      authorize instance_moderation

      message =
        if instance_moderation.public_send(:"may_#{event}?") && instance_moderation.public_send(:"#{event}!")
          :success
        else
          :error
        end

      flash[message] = I18n.t("instance_moderations.#{event}.#{message}")

      redirect_to_moderation_queue!
    end
  end

  def action_on_several
    redirect_to_moderation_queue!

    instance_moderations = site.instance_moderations.where(id: params[:instance_moderation])

    return if instance_moderations.count.zero?

    authorize instance_moderations

    action = params[:instance_moderation_action].to_sym
    method = :"#{action}_all!"

    return unless InstanceModeration.events.include? action

    InstanceModeration.transaction do
      message = instance_moderations.public_send(method) ? :success : :error

      flash[:message] = I18n.t("instance_moderations.action_on_several.#{message}")
    end
  end

  private

  # @return [InstanceModeration]
  def instance_moderation
    @instance_moderation ||= site.instance_moderations.find(params[:instance_moderation_id])
  end
end
