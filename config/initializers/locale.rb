# frozen_string_literal: true

Rails.application.configure do
  config.i18n.available_locales = %i[es en]
  config.i18n.default_locale = :es
end
