# frozen_string_literal: true

# Este metadato permite generar rutas manuales.
class MetadataPermalink < MetadataString
  # Los permalinks nunca pueden ser privados
  def private?
    false
  end

  private

  # Al hacer limpieza, validamos la ruta.  Eliminamos / multiplicadas,
  # puntos suspensivos, la primera / para que siempre sea relativa y
  # agregamos una / al final si la ruta no tiene extensión.
  def sanitize(value)
    value  = value.strip.unicode_normalize.gsub('..', '/').gsub('./', '').squeeze('/')
    value  = value[1..-1] if value.start_with? '/'
    value += '/' if File.extname(value).blank?

    value
  end
end
