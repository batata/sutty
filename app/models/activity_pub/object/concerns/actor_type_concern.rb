# frozen_string_literal: true

class ActivityPub
  class Object
    module Concerns
      module ActorTypeConcern
        extend ActiveSupport::Concern

        included do
          # La URI de le Actor en este caso es la misma id
          #
          # @return [String]
          def actor_uri
            uri
          end

          # El objeto referencia a une Actor
          #
          # @see {https://www.w3.org/TR/activitystreams-vocabulary/#actor-types}
          def actor_type?
            true
          end

          # El objeto es un objeto
          #
          # @see {https://www.w3.org/TR/activitystreams-vocabulary/#object-types}
          def object_type?
            false
          end
        end
      end
    end
  end
end
