# frozen_string_literal: true

# Usuarie de la plataforma
class Usuarie < ApplicationRecord
  include Usuarie::Consent

  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :lockable, :registerable

  validates_uniqueness_of :email
  validates_with EmailAddress::ActiveRecordValidator, field: :email
  validate :locale_available!

  before_create :lang_from_locale!
  before_update :remove_confirmation_invitation_inconsistencies!
  before_update :accept_invitation_after_confirmation!

  has_many :roles
  has_many :sites, through: :roles
  has_many :blazer_audits, foreign_key: 'user_id', class_name: 'Blazer::Audit'
  has_many :blazer_queries, foreign_key: 'creator_id', class_name: 'Blazer::Query'

  self.filter_attributes += [/\Aemail\z/, /\Aencrypted_password\z/]

  def name
    email.split('@', 2).first
  end

  # Encuentra el rol que tiene le usuarie en el sitio
  def rol_for_site(site)
    roles.find_by(site: site)
  end

  # XXX: Ver increment_and_lock
  def can_sign_in?(password)
    active_for_authentication? && valid_password?(password)
  end

  # XXX: Estamos duplicando la forma en que Devise bloquea acceso
  # por intentos fallidos porque no tenemos forma de correr
  # validate() desde la estrategia DatabaseAuthenticatable sin
  # generar una redirección.
  #
  # lib/devise/models/lockable.rb
  def increment_and_lock!
    increment_failed_attempts
    lock_access! if attempts_exceeded? && !access_locked?
  end

  def send_devise_notification(notification, *args)
    I18n.with_locale(lang) do
      devise_mailer.send(notification, self, *args).deliver_later
    end
  end

  # Les usuaries necesitan link de invitación si no tenían cuenta
  # y todavía no aceptaron la invitación anterior.
  def needs_invitation_link?
    created_by_invite? && !invitation_accepted?
  end

  private

  def lang_from_locale!
    self.lang = I18n.locale.to_s
  end

  # El invitation_token solo es necesario cuando fue creade por otre
  # usuarie.  De lo contrario lo que queremos es un proceso de
  # confirmación.
  def remove_confirmation_invitation_inconsistencies!
    self.invitation_token = nil unless created_by_invite?
  end

  # Si le usuarie (re)confirma su cuenta con una invitación pendiente,
  # considerarla aceptada también.
  def accept_invitation_after_confirmation!
    return unless confirmed?

    self.invitation_token = nil
    self.invitation_accepted_at ||= Time.now.utc
  end

  # Muestra un error si el idioma no está disponible al cambiar el
  # idioma de la cuenta.
  #
  # @return [nil]
  def locale_available!
    return if I18n.locale_available? lang

    errors.add(:lang, I18n.t('activerecord.errors.models.usuarie.attributes.lang.not_available'))
    nil
  end
end
