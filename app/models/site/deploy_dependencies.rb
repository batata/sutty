# frozen_string_literal: true

require 'rgl/adjacency'
require 'rgl/topsort'

class Site
  module DeployDependencies
    extend ActiveSupport::Concern

    included do
      # Genera un grafo dirigido de todos los métodos de publicación
      #
      # @return [RGL::DirectedAdjacencyGraph]
      def deployment_graph
        @deployment_graph ||= RGL::DirectedAdjacencyGraph.new.tap do |graph|
          deploys.each do |deploy|
            graph.add_vertex deploy
          end

          deploys.each do |deploy|
            deploy.class.all_dependencies.each do |dependency|
              deploys.where(type: dependency.to_s.classify).each do |deploy_dependency|
                graph.add_edge deploy_dependency, deploy
              end
            end
          end
        end
      end

      # Devuelve una lista ordenada de todos los métodos de publicación
      #
      # @return [Array]
      def deployment_list
        @deployment_list ||= deployment_graph.topsort_iterator.to_a
      end
    end
  end
end
