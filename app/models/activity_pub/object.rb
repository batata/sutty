# frozen_string_literal: true

# Almacena objetos de ActivityPub, como Note, Article, etc.
class ActivityPub
  class Object < ApplicationRecord
    include ActivityPub::Concerns::JsonLdConcern

    before_validation :type_from_content!, unless: :type?

    # Los objetos son únicos a toda la base de datos
    validates :uri, presence: true, url: true, uniqueness: true
    validate :uri_is_content_id?, if: :content?

    has_many :activity_pubs, as: :object

    # Encontrar le Actor por su relación con el objeto
    #
    # @return [ActivityPub::Actor,nil]
    def actor
      ActivityPub::Actor.find_by(uri: actor_uri)
    end

    # @return [String]
    def actor_uri
      content['attributedTo']
    end

    def actor_type?
      false
    end

    def object_type?
      true
    end

    # Poder explorar propiedades remotas
    #
    # @return [DistributedPress::V1::Social::ReferencedObject]
    def referenced(site)
      require 'distributed_press/v1/social/referenced_object'

      @referenced ||= DistributedPress::V1::Social::ReferencedObject.new(object: content,
                                                                         dereferencer: site.social_inbox.dereferencer)
    end

    private

    def uri_is_content_id?
      return if uri == content['id']

      errors.add(:activity_pub_objects, 'El ID del objeto no coincide con su URI')
    end

    # Encuentra el tipo a partir del contenido, si existe.
    #
    # XXX: Si el objeto es una actividad, esto siempre va a ser
    # Generic
    def type_from_content!
      self.type =
        begin
          "ActivityPub::Object::#{content['type'].presence || 'Generic'}".constantize
        rescue NameError
          ActivityPub::Object::Generic
        end
    end
  end
end
