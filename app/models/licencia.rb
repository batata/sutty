# frozen_string_literal: true

# Las licencias son completamente traducibles
class Licencia < ApplicationRecord
  extend Mobility

  translates :name, type: :string, locale_accessors: true
  translates :url, type: :string, locale_accessors: true
  translates :description, type: :text, locale_accessors: true
  translates :short_description, type: :string, locale_accessors: true
  translates :deed, type: :text, locale_accessors: true

  has_many :sites

  validates :name, presence: true, uniqueness: true
  validates :url, presence: true
  validates :description, presence: true
  validates :short_description, presence: true
  validates :deed, presence: true

  def custom?
    icons == 'custom'
  end
end
