# frozen_string_literal: true

class DeployJobTest < ActiveSupport::TestCase
  test 'se puede compilar' do
    rol = create :rol
    site = rol.site
    site.deploys << create(:deploy_zip, site: site)

    site.save

    DeployJob.perform_async(site.id)

    assert_not ActionMailer::Base.deliveries.empty?

    site.deploys.each do |d|
      assert File.exist?(d.respond_to?(:path) ? d.path : d.destination)
    end

    site.destroy
  end
end
