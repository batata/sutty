class AddApiKeyToSites < ActiveRecord::Migration[6.0]
  def up
    add_column :sites, :api_key_ciphertext, :string

    begin
      Site.find_each do |site|
        site.update_attribute :api_key, SecureRandom.hex(64)
      end
    rescue Errno::ENOENT
    end
  end

  def down
    remove_column :sites, :api_key_ciphertext
  end
end
