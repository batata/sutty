# frozen_string_literal: true

# Agrega relaciones en las remote flags
class ChangeRemoteFlags < ActiveRecord::Migration[6.1]
  def up
    add_column :activity_pubs, :remote_flag_id, :uuid, index: true, null: true
  end

  def down
    remove_column :activity_pubs, :remote_flag_id
  end
end
