# frozen_string_literal: true

# Una clase de I18n solo necesaria para Pundit
#
# ver app/policies/i18n.rb
class SiteTranslation
  attr_reader :site

  def initialize(site)
    @site = site
  end
end
