# frozen_string_literal: true

require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rol = create :rol
    @site = @rol.site
    @usuarie = @rol.usuarie
    @post = @site.posts.build(title: SecureRandom.hex,
                              description: SecureRandom.hex)
    @post.save

    @authorization = {
      Authorization: ActionController::HttpAuthentication::Basic
                     .encode_credentials(@usuarie.email, @usuarie.password)
    }
  end

  teardown do
    @site.destroy
  end

  test 'se pueden ver todos' do
    get site_posts_url(@site), headers: @authorization

    assert_match @site.name, response.body
    assert_match @post.title.value, response.body
  end

  test 'se pueden crear nuevos' do
    title = SecureRandom.hex
    post site_posts_url(@site), headers: @authorization,
                                params: {
                                  post: {
                                    title: title,
                                    description: title,
                                    date: 2.days.ago.strftime('%F')
                                  }
                                }

    # TODO: implementar reload?
    site = Site.find(@site.id)
    new_post = site.posts.find_by(title: title)

    assert_equal 302, response.status

    # XXX: No usamos follow_redirect! porque pierde la autenticación
    get site_posts_url(@site), headers: @authorization

    assert_match new_post.title.value, response.body
    assert_equal title, new_post.title.value
    assert_equal I18n.t('post_service.created', title: new_post.title.value),
                 @site.repository.rugged.head.target.message
  end

  test 'se pueden ver' do
    get site_post_url(@site, @post.lang.value, @post.id), headers: @authorization

    assert_equal 200, response.status
    assert_match @post.title.value, response.body
  end

  test 'se pueden actualizar' do
    title = SecureRandom.hex

    patch site_post_url(@site, @post.lang.value, @post.id),
          headers: @authorization,
          params: {
            post: {
              title: title,
              description: title
            }
          }

    assert_equal 302, response.status

    get site_posts_url(@site), headers: @authorization

    assert_match title, response.body

    site = Site.find @site.id
    assert site.posts.map { |p| p.title.value }.include?(title)
  end

  test 'se pueden eliminar' do
    params = ActionController::Parameters.new(post: { author: ['hola'] })
    assert PostService.new(site: @site,
                           usuarie: @usuarie,
                           post: @post,
                           params: params).update

    delete site_post_url(@site, @post.lang.value, @post.id), headers: @authorization
    get site_posts_url(@site), headers: @authorization

    site = Site.find @site.id

    assert_not site.posts.include?(@post)
    assert @post.destroyed?
    assert_equal I18n.t('post_service.destroyed', title: @post.title.value),
                 @site.repository.rugged.head.target.message
  end

  test 'se pueden subir imágenes' do
    patch site_post_url(@site, @post.lang.value, @post.id),
          headers: @authorization,
          params: {
            post: {
              image: {
                path: fixture_file_upload('logo.png', 'image/png'),
                description: 'hola'
              }
            }
          }

    assert_equal 302, response.status

    @site = Site.find(@site.id)
    # TODO: Implementar reload
    @post = @site.posts.find(@post.id)

    Dir.chdir(@site.path) do
      assert File.exist?(@post.image.value['path'])
    end

    assert_equal 'hola', @post.image.value['description']
  end

  test 'no se pueden subir archivos cualquiera' do
    patch site_post_url(@site, @post.lang.value, @post.id),
          headers: @authorization,
          params: {
            post: {
              image: {
                path: fixture_file_upload('_logo.png', 'image/png'),
                description: 'hola'
              }
            }
          }

    assert_equal 200, response.status
    assert_match I18n.t('metadata.image.not_an_image'), response.body
  end

  test 'se pueden reordenar' do
    lang = { lang: @site.locales.sample }

    (rand * 10).round.times do
      @site.posts(**lang).create title: SecureRandom.hex, description: SecureRandom.hex
    end

    posts = @site.posts(**lang)
    reorder = Hash[posts.map { |p| p.uuid.value }.shuffle.each_with_index.to_a]

    post site_posts_reorder_url(@site),
         headers: @authorization,
         params: { post: { lang: lang[:lang], reorder: reorder } }

    @site = Site.find @site.id

    assert_equal reorder,
                 Hash[@site.posts(**lang).map do |p|
                   [p.uuid.value, p.order.value]
                 end]
    assert_equal I18n.t('post_service.reorder'),
                 @site.repository.rugged.head.target.message
  end
end
