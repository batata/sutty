# Plantillas de artículos

Las plantillas de artículos son archivos YML con la estructura de campos
a utilizar en la creación de un artículo.  Dependiendo del tipo de
artículo que elijamos, el editor del artículo cargará los campos
necesarios.

Las plantillas residen en la colección `_templates` como archivos
markdown.  Esto permite que Jekyll los cargue como una colección a
través de esa feature ya implementada, reciclando código.

Además, permite que las plantillas tengan, además de una estructura de
datos YAML por defecto, un cuerpo por defecto, si quisiéramos tener una
estructura del documento también.

## Estructura del documento

```
---
title: ""
categories: []
tags: []
related_articles: []
destacado: false
resumen: ""
descripcion_detallada: ""
---

# Título de primer nivel

Un resumen que quieras poner aquí.  Esto irá en el editor de texto.

```

Entonces, en un primer momento, el formulario se arma en base a los
tipos de datos que se espera en un campo.  Si el valor es `""`, se
espera un campo de texto, si es `[]`, se espera un array y así.

Pero:

* Cómo distinguir entre un campo de texto largo y uno más corto?

* El campo `related_articles` debería ser autocompletado a partir de los
  títulos de otros artículos cargados, entonces cómo indicar esto?

La conversión de cyber-women.com debería ser así:

```
---
categories: []
cover: ""
lang:
  es: ""
  en: ""
  ar: ""
layout: ""
title: ""
objetivos: ""
tags: []
permalink: ""
dir: ""
---
```

Pero acá no se puede distinguir que en realidad en `cover` y en
`permalink` van paths, que los idiomas se autocompletan con titulos de
otra colección, que `tags` y `categories` se autocompletan con valores
que hay en otros artículos y que `dir` es uno de `ltr` o `rtl`.

`dir` es fácil, se pone un array con los valores que van:

```
---
categories: []
cover: ""
lang:
  es: ""
  en: ""
  ar: ""
layout: ""
title: ""
objetivos: ""
tags: []
permalink: ""
dir: [ ltr, rtl ]
---
```

La otra opción es hacer una lista de las propiedades que se
autocompletan, por ejemplo, los campos estándar:

* `categories`
* `tags`
* `cover`
* `layout`
* `dir`

Ya tienen programáticamente sus valores.  Pero sería laburo doble porque
el editor de plantillas tiene que poder crear campos arbitrarios con
valores predeterminados.

Aunque el editor de plantillas todavía no lo necesitamos hacer?


```
---
categories: [site/categories]
cover: string
lang:
  es: en/title:en/slug
  en: es/title:es/slug
  ar: ar/title:ar/slug
layout: site/layouts
title: string
objetivos: text
tags: [site/tags]
permalink: string
dir: [ ltr, rtl ]
---
```

Donde `site/categories` equivale a `@site.everything_of('categories')` y
`en/slug` a `@site.everything_of('slug', lang: 'en')`.

Al separar con `:`, estamos diciendo qué valores mostrar y a cuáles
mapearlos, entonces `en/slug:en/title` muestra un selector de todos los
títulos, pero almacena el slug correspondiente.

Para saber si un valor es un array, encontramos un array y miramos
dentro qué tipos de valores vamos a rellenar.

luego, el editor de plantillas de posts permite crear estos tipos de
campos con un formulario dinamico

Las plantillas tienen un layout correspondiente.  Luego se puede sugerir
y que las usuarias lo cambian.

No es necesario definir los campos estandar `categories`, `lang`,
`layout`, `title`, `tags`, `slug`, `permalink` y `dir`.


Finalmente, solo es necesario definir los campos extra:

```
---
cover: string
objetivos: text
habilidades: 'posts/habilidades'
conocimientos: ['posts/conocimientos']
sesiones_ejercicios_relacionados: ['posts/slug:posts/title']
destacado: false
---
```

Donde `string` y `text` indican valores de texto de distintos largos
(`text` y `textarea` en el formulario), `false` un valor binario (un
`checkbox`) y `coleccion/valor` y
`coleccion/valor:coleccion/valor_a_mostrar` en valores tomados de otras
colecciones.  Cuando están entre `[]`, permite múltiples valores.

Entonces, para resumir:

* La plantilla se coloca en `_templates/nombre_de_la_plantilla.markdown`
* Es un documento Jekyll con _frontmatter_ y contenido
* El _frontmatter_ trae los campos extra a completar desde la carga de
  artículos
* El `slug` de la plantilla se convierte en el `layout` sugerido del
  post, es decir que debería existir un
  `_layouts/nombre_de_la_plantilla.html` correspondiente.
* El `_config.yml` del sitio debe contener:

```yaml
collections:
  - templates
```

  Para poder cargar la colección de plantillas.

  TODO: agregarlo especificamente en `app/models/site.rb` al cargar el
  sitio.

## Plantillas complejas

Algunas plantillas necesitan comportamientos más complejos, por ejemplo
cuando un campo es requerido o no es público.

```
campo:
  help: "Un texto de ayuda"
  public: false
  required: true
  value: {bool,string,text,array,url,email}
  min: 0
  max: 100
  multiple: false
```

Ahora los `value` pueden estar anidados, es decir que dentro de cada
value puede haber una serie de `campo` con los mismos valores.

Las plantillas más simples asumen ciertos valores por defecto:

```
help: nil
public: true
required: false
min: nil
max: nil
multiple: true
open: true
```
