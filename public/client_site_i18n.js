const clientSideI18n = () => {
  const available_locales = ['es','en']
  const current_locale = navigator.languages.map(l => l.split('-')[0]).find(l => available_locales.includes(l))

  if (!current_locale) current_locale = available_locales[0]

  available_locales.forEach(locale => {
    if (locale == current_locale) return

    document.querySelectorAll(`[lang=${locale}]`).forEach(el => el.remove())
  })
}

document.addEventListener('DOMContentLoaded', clientSideI18n)
document.addEventListener('turbolinks:load', clientSideI18n)
