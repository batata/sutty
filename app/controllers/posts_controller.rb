# frozen_string_literal: true

# Controlador para artículos
class PostsController < ApplicationController
  before_action :authenticate_usuarie!
  before_action :service_for_direct_upload, only: %i[new edit]

  # TODO: Traer los comunes desde ApplicationController
  breadcrumb -> { current_usuarie.email }, :edit_usuarie_registration_path
  breadcrumb 'sites.index', :sites_path, match: :exact
  breadcrumb -> { site.title }, -> { site_posts_path(site, locale: locale) }, match: :exact

  # Las URLs siempre llevan el idioma actual o el de le usuarie
  def default_url_options
    { locale: locale }
  end

  def index
    authorize Post

    # XXX: Cada vez que cambiamos un Post tocamos el sitio con lo que es
    # más simple saber si hubo cambios.
    return unless stale?([current_usuarie, site, filter_params])

    # Todos los artículos de este sitio para el idioma actual
    @posts = site.indexed_posts.where(locale: locale)
    @posts = @posts.page(filter_params.delete(:page)) if site.pagination
    # De este tipo
    @posts = @posts.where(layout: filter_params[:layout]) if filter_params[:layout]
    # Que estén dentro de la categoría
    @posts = @posts.in_category(filter_params[:category]) if filter_params[:category]
    # Aplicar los parámetros de búsqueda
    @posts = @posts.search(locale, filter_params[:q]) if filter_params[:q].present?
    # A los que este usuarie tiene acceso
    @posts = PostPolicy::Scope.new(current_usuarie, @posts).resolve

    # Filtrar los posts que les invitades no pueden ver
    @usuarie = site.usuarie? current_usuarie

    @site_stat = SiteStat.new(site)
  end

  def show
    authorize post
    breadcrumb post.title.value, ''
    fresh_when post
  end

  # Genera una previsualización del artículo.
  def preview
    authorize post

    render html: post.render
  end

  def new
    authorize Post
    @post = site.posts(lang: locale).build(layout: params[:layout])

    breadcrumb I18n.t('loaf.breadcrumbs.posts.new', layout: @post.layout.humanized_name.downcase), ''
  end

  def create
    authorize Post
    service = PostService.new(site: site,
                              usuarie: current_usuarie,
                              params: params)
    @post = service.create

    if @post.persisted?
      site.touch
      forget_content

      redirect_to site_post_path(@site, @post)
    else
      render 'posts/new'
    end
  end

  def edit
    authorize post
    breadcrumb post.title.value, site_post_path(site, post, locale: locale), match: :exact
    breadcrumb 'posts.edit', ''
  end

  def update
    authorize post

    service = PostService.new(site: site,
                              post: post,
                              usuarie: current_usuarie,
                              params: params)

    if service.update.persisted?
      site.touch
      forget_content

      redirect_to site_post_path(site, post)
    else
      render 'posts/edit'
    end
  end

  # Eliminar artículos
  def destroy
    authorize post

    service = PostService.new(site: site,
                              post: post,
                              usuarie: current_usuarie,
                              params: params)

    # TODO: Notificar si se pudo o no
    service.destroy
    site.touch
    redirect_to site_posts_path(site, locale: post.lang.value)
  end

  # Reordenar los artículos
  def reorder
    authorize site

    service = PostService.new(site: site,
                              usuarie: current_usuarie,
                              params: params)

    service.reorder
    site.touch
    redirect_to site_posts_path(site, locale: site.default_locale)
  end

  # Devuelve el idioma solicitado a través de un parámetro, validando
  # que el sitio soporte ese idioma, de lo contrario devuelve el idioma
  # actual.
  #
  # TODO: Debería devolver un error o mostrar una página donde se
  # solicite a le usuarie crear el nuevo idioma y que esto lo agregue al
  # _config.yml del sitio en lugar de mezclar idiomas.
  def locale
    @locale ||= site&.locales&.find(-> { site&.default_locale }) do |l|
      l.to_s == params[:locale]
    end
  end

  # Instruye al editor a olvidarse el contenido del artículo.  Usar
  # cuando hayamos guardado la información correctamente.
  def forget_content
    flash[:js] = { target: 'editor', action: 'forget-content', keys: (params[:storage_keys] || []).to_json }
  end

  private

  # Los parámetros de filtros que vamos a mantener en todas las URLs,
  # solo los que no estén vacíos.
  #
  # @return [Hash]
  def filter_params
    @filter_params ||= params.permit(:q, :category, :layout, :page).to_hash.select do |_, v|
      v.present?
    end.transform_keys(&:to_sym)
  end

  def post
    @post ||= site.posts(lang: locale).find(params[:post_id] || params[:id])
  end

  # Recuerda el nombre del servicio de subida de archivos
  def service_for_direct_upload
    session[:service_name] = site.name.to_sym
  end
end
