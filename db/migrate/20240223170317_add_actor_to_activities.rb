# frozen_string_literal: true

# Relaciona Actor con Activity
class AddActorToActivities < ActiveRecord::Migration[6.1]
  def up
    add_column :activity_pub_activities, :actor_id, :uuid, index: true

    ActivityPub::Activity.find_each do |activity|
      actor = ActivityPub::Actor.find_by(uri: activity.content['actor'])

      activity.update(actor: actor) if actor.present?
    end
  end

  def down
    remove_column :activity_pub_activities, :actor_id, :uuid, index: true
  end
end
