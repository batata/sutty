# frozen_string_literal: true

# = Activity =
#
# Lleva un registro de las actividades que nos piden hacer remotamente.
#
# Las actividades pueden tener distintos destinataries (sitios/actores).
#
# @todo Obtener el contenido del objeto dinámicamente si no existe
# localmente, por ejemplo cuando la actividad crea un objeto pero lo
# envía como referencia en lugar de anidarlo.
#
# @see {https://www.w3.org/TR/activitypub/#client-to-server-interactions}
class ActivityPub
  class Activity < ApplicationRecord
    include ActivityPub::Concerns::JsonLdConcern

    belongs_to :activity_pub
    belongs_to :actor, touch: true
    has_one :object, through: :activity_pub

    validates :activity_pub_id, presence: true
    # Las actividades son únicas con respecto a su estado
    validates :uri, presence: true, url: true, uniqueness: { scope: :activity_pub_id, message: 'estado duplicado' }

    # Siempre en orden descendiente para saber el último estado
    default_scope -> { order(created_at: :desc) }

    # Cambia la máquina de estados según el tipo de actividad
    def update_activity_pub_state!
      nil
    end
  end
end
